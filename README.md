# LINCS ResearchSpace

This project customises the official ResearchSpace platform by adding a ResearchSpace *app*. This is done by adding custom files to the `/app` folder of the base ResearchSpace Docker image. Note that the base ResearchSpace and Blazegraph images are built with x86 architecture only and should be run with the `platform: linux/amd64` specified in Docker Compose for M1/2/3 Macs that default to Arm64 architecture.

The project also includes a Helm chart to deploy the entire RS platform to LINCS infrastructure with CI/CD automation.

For local development, mount the `lincs` folder inside the `/apps` folder using Docker Compose.

## Run with Docker Compose

Modify the permissions of the `./researchspace` directory recursively so that any user has write and read access:

```bash
sudo chmod 777 researchspace -R
```

Then launch the project with docker compose:

```bash
docker compose up
```

The service can be found at [http://localhost:8080/](http://localhost:8080/)

Following first time launch, install the generic containers (found in `./generic-containers`). Login with `admin:admin` and then click the settings wheel and then the *LDP* card. This will open a window where you can upload the generic containers.

## Update mirror site data (Longhorn volume restore)

Follow these instructions to sync the LINCS mirror site with the LINCS production deployment.

Before starting, ensure that the correct version of the project is deployed in `mirror` by hitting the play button for the *mirror* deploy on the appropriate CD/CD pipeline.

The data restore process basically involves shutting down pods and deleting the current volumes in `mirror`, then restoring the volumes from S3 backups made by `production`, and then starting the pods back up with the new volumes. The steps are detailed out below:

* Log into Rancher and open the mirror site. Go to the Longhorn menu item to open the Longhorn UI in a new window.
* Back in the *Rancher* window, set the filter to the `lincs-rs-custom-app-*` namespace.
* Navigate to Workload -> Deployments.
* Scale down the `blazegraph`, `cantaloupe`, and `mirror` deployments to 0 using the drop-down slider to the right. This will scale down the deployments and delete the pods so that the volumes can be released.
* Navigate to Storage -> PersistentVolumeClaims.
* Delete the `blazegraph-pvc`, `images-pvc`, and `platform-pvc` claims.
* Open the Longhorn app from the Rancher menu and navigate to the Volumes tab. Check to see if rs-blazegraph, rs-cantaloupe, and rs-platform are still present. If they are, select all three and delete.
* Select the *Backup* tab. It will list all the volumes that are currently backed up on S3.
* Find the Volumes named `rs-blazegraph`, `rs-images`, and `rs-platform` that we need to restore. To restore a volume, hover over the menu icon of the volume, and select `Restore Latest Backup`. A form will pop up. Tick the `Use Previous Name` option. Keep number of replicas at 1. Select ReadWriteOnce AccessMode and leave the other fields empty. Do this for all three volumes.
* Now go to the *Volume* tab in the Longhorn UI. Here you will see the three volumes you just restored. But they are detached. We will need to create PVCs for these volumes, so that the pods can use them once we start them up again.
* For each volume, hover over the menu icon on the right and select `Create PV/PVC`. A form will pop up. Make sure that the `Use Previous Name` options is selected. In the namespace field, replace *production* with *mirror*. Leave all other values as-is. Click `OK`.
* Back in the *Rancher* window, go to Workload -> Deployments again and change the replicas of the three deployments back to 1. Be sure to scale Blazegraph up first.
* The deployments should spin up the pods that should mount the new volumes automatically.

## Developing locally

All LINCS RS development happens offline, on a local Docker instance. Ontology files and knowledge patterns are stored as trig files in the lincs app under `ldp/assets`. For each knowledge pattern developed locally, a trig file is created in the `runtime-data/ldp/assets` folder. In order to migrate newly developed knowledge patterns to the stage environment, the new trig file needs to be moved from its location in `runtime-data/ldp/assets` to the `ldp/assets` folder in the lincs app. When merged into the remote repository and redeployed, the new LDP asset will be automatically loaded into the new instance, along with all of the pre-existing knowledge patterns that are stored permanently in the `ldp/assets` folder. This folder is the source of truth for all knowledge patterns, no matter where the deployment occurs. To remove a knowledge pattern from the deployment, simply delete it from the `ldp/assets` folder.

## Authoring entities

In LINCS: Researchspace, entities can be created or modified by permitted users. Typically, permitted users are delegated by the researcher responsible for the data. The process for delegating a new user follows:

1. Register a user in RS
2. Request to have the admin increase user's permissions to `editor`
3. The admin will also add the identity to the metadata graph (`http://metadata.lincsproject.ca`) using the [Metadata Form](http://rs.stage.lincsproject.ca/resource/MetadataForm.html), or manually. The following triples are added:
```text
lincs:6b577167-4344-4f66-b6d4-4855d2cb5580 a crm:E21_Person ;
crm:P2_has_Type lincs:researcher ;
rdfs:label "John Dee" .
owl:sameAs <http://www.researchspace.org/resource/user/johnDee> .
lincs:writesTo <http://graph.lincsproject.ca/adarchive> .
```
4. The user `johnDee` can now create or modify entities belonging to the adarchive graph.

### Editing Metadata

Use the Entity Editor available from the admin page.

## Permissions

There are four roles currently set up in ResearchSpace's Keycloak service and one user set up in ResearchSpace's local login system:

* **Guest:**
    This role is the anonymous user of the application. The is the role you are automatically assigned when you first reach the website and before you login. The Guest has the ability to view resources, search, and query.

* **Contributor:**
    This role is automatically assigned to newly registered users. This is the default authenticated role that anyone who decides to register for a free account gets assigned. The Contributor has the ability to save resources to clipboard and create semantic narratives that save in an LDP container under their username.

* **Editor:**
    This role is the first level of manually raised permissions. Editors have the ability to author new data and edit pre-existing data. In order to be able to access these forms however, the user must also be specified in the permissions graph as having write access for a specific dataset.

* **Administrator:**
    This role is the highest level of permissions. Administrators have the ability to change system configurations, view logs, create and edit templates, restart the service, and have access to all features of the platform.

* **Curl User:**
    This user has the same permissions level as the admin, but can be accessed through basic auth. This authentication is handled through ResearchSpace's local login system (not keycloak). The username is admin by default and the unique password for the user can be set as an env variable.

Note for assigning permissions:
    Since the Guest and Contributor roles are default roles for keycloak users, these roles cannot be removed. This means that to create an admin or an editor, just add the admin **or** editor role to the user to increase permissions (you don't need to assign both, the admin role can do everything an editor can).

### Changing Role Scopes

Changing the scope of a specific user can be done in the `/lincs/config/shiro.ini` file. The descriptions of possible permissions are defined in ResearchSpace documentation.

### Authorship form automation

Authorship forms can be updated semi-automatically by running `generate_form_templates.py`, which depends on `forms_template_manifest.json` (the manifest). The manifest declares which classes exist in the data, and what form elements should be added for each class. You can update a specific class in the manifest, specifically when you want to add specific properties that extends the basic form implemented by the script.

When new classes have been added to the triplestore, use the `update_form_manifest.py` file to fetch new class types from the stage triplestore and add them to your manifest file. A new manifest file is generated in the base folder. Just delete the old manifest file and rename the generated file to match the old name (i.e. rename the generated json file as `forms_template_manifest.json`).

Run `generate_form_templates.py` to regenerate all forms. These will be added straight to the templates folder. Running this file will also generate a new type config map for connecting entity types to their editing forms. Use the new file to replace the named graph at `<http%3A%2F%2Fwww.researchspace.org%2Fresource%2Fsystem%2Fvocab%2Fauthority_manager_config_types>`.
