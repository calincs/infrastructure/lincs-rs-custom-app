from SPARQLWrapper import SPARQLWrapper, RDFXML, POST, GET, JSON
import re
from datetime import datetime
import os
import requests

targetLookup = {
  "ORIGIN": {
    "ENDPOINT": "https://bg-review.lincsproject.ca/sparql",
    "USER": "lincs",
    "PW": os.environ.get("BG_LINCS_PASSWORD")
    },
    "DESTINATION": {
    "ENDPOINT": "https://blazegraph.lincsproject.ca/sparql",
    "USER": "lincs",
    "PW": os.environ.get("BG_LINCS_PASSWORD")
    }    
}


def main():

    """Queries metadata for named graphs that have "published" status. For each graph, pulls metadata and data from the origin endpoint, and writes it to the destination"""

    #get graph names in "published status"
    graphs = queryHelper("SELECT", "ORIGIN", "SELECT distinct ?graph WHERE { graph <http://metadata.lincsproject.ca> { ?graph <http://id.lincsproject.ca/status> <http://id.lincsproject.ca/published> } }")
       
    print("\n{} graphs queued for migration".format(len(graphs["results"]["bindings"])))
    
    #for each graph in published status, migrate the graph
    for graph in graphs["results"]["bindings"]:        
        
        GRAPH = graph['graph']['value']

        print('\nMIGRATING {}...'.format(GRAPH))
        print("\nRecording publication event in origin metadata")

        ids = requests.get("https://uid.lincsproject.ca/generatemultipleUID?num=2").json()['uids']
        dto = "http://id.lincsproject.ca/{}".format(ids[0])
        ts = "http://id.lincsproject.ca/{}".format(ids[1])        
        time = str(datetime.now())

      
        metadata_query = """PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> INSERT { graph <http://metadata.lincsproject.ca> { <""" + GRAPH + """> crm:P2_has_type <http://id.lincsproject.ca/prepublication> . <""" + dto + """> a crmdig:D12_Data_Transfer_Event ; crmdig:L14_was_transferred <""" + GRAPH + """> ; rdfs:label \"Data transfer to production: """ + time + """\" ; crm:P4_has_time-span <""" + ts + """> . <""" + ts + """> a crm:E52_Time-Span ; crm:P82_at_some_time_within \"""" + time + """\" ; rdfs:label "Timespan for data transfer to production: """ + time + """\" . . } } WHERE { }"""
      
        result = queryHelper("UPDATE", "ORIGIN", metadata_query)
        print("\nREPLACING WITH PRE-PUBLICATION STATUS: ", parseResult(result))

        metadata_construct = """CONSTRUCT { 
		?graph crm:P1_is_identified_by ?id ;
      		crm:P104_is_subject_to ?rights ;
        	crm:P2_has_type ?status ;
      		crm:P3_has_note ?note .

  		?id ?id_predicate ?id_object .
  
  		?description crm:P129_is_about ?graph ;
  			a ?description_class ;
  			crm:P2_has_type ?description_type ;
  			crm:P190_has_symbolic_content ?description_content .
  
    	?source crm:P130_shows_features_of ?graph ;
  			crm:P1_is_identified_by ?source_id .
  		?source_id ?source_predicate ?source_object .
  		?creation crmdig:L21_used_as_derivation_source ?source ;
  			crm:P14_carried_out_by ?actor .
  		?actor ?actor_predicate ?actor_object .
  		?role crm:P01_has_domain ?creation ;
  			?role_predicate ?role_object .
        
  		?dte crmdig:L14_was_transferred ?graph ;
              ?dte_predicate ?dte_object ;
              crm:P4_has_time-span ?dte_timespan .
        ?dte_timespan ?dte_timespan_predicate ?dte_timespan_object .

  		?graph crm:P129_is_about ?period .
  		?period crm:P4_has_time-span ?time .
      	?period ?period_predicate ?period_object .
  		?time ?time_predicate ?time_object .
  		
      	?graph crm:P106_is_composed_of ?sub_graph .
	  	
		  ?graph rs:PX_has_main_representation ?main_representation .
      		?representation crm:P138_represents ?graph ;
  				a ?rep_class ;
      			crm:P2_has_type ?rep_type ;
  				crm:P1_is_identified_by ?rep_location .
      	
  		?annotation crmdig:L43_annotates ?graph ;
  			?annotation_predicate ?annotation_object .
		
        } WHERE {
        graph <http://metadata.lincsproject.ca> { 
      	# all graphs have at least one id, a license, a status.
		<"""  + GRAPH + """> crm:P1_is_identified_by ?id ;
      		crm:P104_is_subject_to ?rights ;
        	crm:P2_has_type ?status  .
  		?id ?id_predicate ?id_object .	
  		
  		# descriptions and affiliated website
  		?description crm:P129_is_about <"""  + GRAPH + """> ;
  			a ?description_class ;
  			crm:P2_has_type ?description_type ;
  			crm:P190_has_symbolic_content ?description_content .
  	
  		# source info
  		?source crm:P130_shows_features_of <"""  + GRAPH + """> .
  		?creation crmdig:L21_used_as_derivation_source ?source ;
  			crm:P14_carried_out_by ?actor .
  		?actor ?actor_predicate ?actor_object .
  		?role crm:P01_has_domain ?creation ;
  			?role_predicate ?role_object .
    
    	OPTIONAL {
    		?creation crmdig:L21_used_as_derivation_source ?source ;
      			crm:P1_is_identified_by ?source_id .
  			?source_id ?source_predicate ?source_object .
  		}
  		# data transfers
  		OPTIONAL {
          ?dte crmdig:L14_was_transferred <"""  + GRAPH + """> ;
              ?dte_predicate ?dte_object ;
              crm:P4_has_time-span ?dte_timespan .
          ?dte_timespan ?dte_timespan_predicate ?dte_timespan_object .
		}
  		
  	  	# Period
  		OPTIONAL {
  			<"""  + GRAPH + """> crm:P129_is_about ?period .
  			?period crm:P4_has_time-span ?time .
      		?period ?period_predicate ?period_object .
  			?time ?time_predicate ?time_object .
	  	}
  
  		# subgraph(s)
  	  	OPTIONAL {
  			<"""  + GRAPH + """> crm:P106_is_composed_of ?sub_graph .
	  	}
  
  		# images
  	  	OPTIONAL {
  			<"""  + GRAPH + """> rs:PX_has_main_representation ?main_representation .
      		?representation crm:P138_represents <"""  + GRAPH + """> ;
  				a ?rep_class ;
      			crm:P2_has_type ?rep_type ;
  				crm:P1_is_identified_by ?rep_location .
      	}
      	  
     	# notes
  		OPTIONAL {
  	  		<"""  + GRAPH + """> crm:P3_has_note ?note .      
    	}
    
  		# annotations
  		OPTIONAL {
  			?annotation crmdig:L43_annotates <"""  + GRAPH + """> ;
  				?annotation_predicate ?annotation_object .
		}
  		BIND(<"""  + GRAPH + """> as ?graph)}}"""

        metadata_delete = """DELETE { graph <http://metadata.lincsproject.ca> { 
		?graph crm:P1_is_identified_by ?id ;
      		crm:P104_is_subject_to ?rights ;
        	crm:P2_has_type ?status ;
      		crm:P3_has_note ?note .

  		?id ?id_predicate ?id_object .
  
  		?description crm:P129_is_about ?graph ;
  			a ?description_class ;
  			crm:P2_has_type ?description_type ;
  			crm:P190_has_symbolic_content ?description_content .
  
    	?source crm:P130_shows_features_of ?graph ;
  			crm:P1_is_identified_by ?source_id .
  		?source_id ?source_predicate ?source_object .
  		?creation crmdig:L21_used_as_derivation_source ?source ;
  			crm:P14_carried_out_by ?actor .
  		?actor ?actor_predicate ?actor_object .
  		?role crm:P01_has_domain ?creation ;
  			?role_predicate ?role_object .
        
  		?dte crmdig:L14_was_transferred ?graph ;
              ?dte_predicate ?dte_object ;
              crm:P4_has_time-span ?dte_timespan .
        ?dte_timespan ?dte_timespan_predicate ?dte_timespan_object .

  		?graph crm:P129_is_about ?period .
  		?period crm:P4_has_time-span ?time .
      	?period ?period_predicate ?period_object .
  		?time ?time_predicate ?time_object .
  		
      	?graph crm:P106_is_composed_of ?sub_graph .
	  	
		  ?graph rs:PX_has_main_representation ?main_representation .
      		?representation crm:P138_represents ?graph ;
  				a ?rep_class ;
      			crm:P2_has_type ?rep_type ;
  				crm:P1_is_identified_by ?rep_location .
      	
  		?annotation crmdig:L43_annotates ?graph ;
  			?annotation_predicate ?annotation_object .
} } WHERE {
    graph <http://metadata.lincsproject.ca> { 
      	# all graphs have at least one id, a license, a status.
		<"""  + GRAPH + """> crm:P1_is_identified_by ?id ;
      		crm:P104_is_subject_to ?rights ;
        	crm:P2_has_type ?status  .
  		?id ?id_predicate ?id_object .	
  		
  		# descriptions and affiliated website
  		?description crm:P129_is_about <"""  + GRAPH + """> ;
  			a ?description_class ;
  			crm:P2_has_type ?description_type ;
  			crm:P190_has_symbolic_content ?description_content .
  	
  		# source info
  		?source crm:P130_shows_features_of <"""  + GRAPH + """> .
  		?creation crmdig:L21_used_as_derivation_source ?source ;
  			crm:P14_carried_out_by ?actor .
  		?actor ?actor_predicate ?actor_object .
  		?role crm:P01_has_domain ?creation ;
  			?role_predicate ?role_object .
    
    	OPTIONAL {
    		?creation crmdig:L21_used_as_derivation_source ?source ;
      			crm:P1_is_identified_by ?source_id .
  			?source_id ?source_predicate ?source_object .
  		}
  		# data transfers
  		OPTIONAL {
          ?dte crmdig:L14_was_transferred <"""  + GRAPH + """> ;
              ?dte_predicate ?dte_object ;
              crm:P4_has_time-span ?dte_timespan .
          ?dte_timespan ?dte_timespan_predicate ?dte_timespan_object .
		}
  		
  	  	# Period
  		OPTIONAL {
  			<"""  + GRAPH + """> crm:P129_is_about ?period .
  			?period crm:P4_has_time-span ?time .
      		?period ?period_predicate ?period_object .
  			?time ?time_predicate ?time_object .
	  	}
  
  		# subgraph(s)
  	  	OPTIONAL {
  			<"""  + GRAPH + """> crm:P106_is_composed_of ?sub_graph .
	  	}
  
  		# images
  	  	OPTIONAL {
  			<"""  + GRAPH + """> rs:PX_has_main_representation ?main_representation .
      		?representation crm:P138_represents <"""  + GRAPH + """> ;
  				a ?rep_class ;
      			crm:P2_has_type ?rep_type ;
  				crm:P1_is_identified_by ?rep_location .
      	}
      	  
     	# notes
  		OPTIONAL {
  	  		<"""  + GRAPH + """> crm:P3_has_note ?note .      
    	}
    
  		# annotations
  		OPTIONAL {
  			?annotation crmdig:L43_annotates <"""  + GRAPH + """> ;
  				?annotation_predicate ?annotation_object .
		}
  		BIND(<"""  + GRAPH + """> as ?graph)} }"""

        # get metadata from origin (i.e. Rs-review)
         
        metadata = queryHelper("CONSTRUCT", "ORIGIN", metadata_construct)

        # delete metadata in destination
        result = queryHelper("UPDATE", "DESTINATION", metadata_delete)
        print("\nREMOVING EXISTING METADATA FROM DESTINATION: ", parseResult(result))
        
        #insert metadata into the destination graph
        result = queryHelper("UPDATE", "DESTINATION", createUpdateQuery(metadata, "http://metadata.lincsproject.ca"))
        print("\nINSERTING METADATA FOR {} INTO DESTINATION: ".format(GRAPH), parseResult(result))

        #drop existing data from destination graph
        result = queryHelper("UPDATE", "DESTINATION", "DROP GRAPH <{}>".format(GRAPH))
        print("\nREMOVING EXISTING DATA FROM DESTINATION: ", parseResult(result))

        #get graph data from origin (i.e. Rs-review)
        data = queryHelper("CONSTRUCT", "ORIGIN", "CONSTRUCT {?s ?p ?o} WHERE { GRAPH <" + GRAPH + "> { { ?s ?p ?o } } }")   
              
        #insert data into the destination graph
        result = queryHelper("UPDATE", "DESTINATION", createUpdateQuery(data, GRAPH))
        print("\nINSERTING DATA FOR {} INTO DESTINATION: ".format(GRAPH), parseResult(result))

        #revert metadata status back to pre-publication in the origin graph
        result = queryHelper("UPDATE", "ORIGIN", "DELETE { graph <http://metadata.lincsproject.ca> { <" + GRAPH + "> crm:P2_has_type <http://id.lincsproject.ca/published> } } WHERE { graph <http://metadata.lincsproject.ca> { <" + GRAPH + "> <http://id.lincsproject.ca/status> <http://id.lincsproject.ca/published> } }")    
        print("\nDROPING PUBLISHED STATUS FROM ORIGIN METADATA: ", parseResult(result))
        
        #remove published status from production
        result = queryHelper("UPDATE", "DESTINATION", "DELETE { graph <http://metadata.lincsproject.ca> { <" + GRAPH + "> crm:P2_has_type <http://id.lincsproject.ca/published>, <http://id.lincsproject.ca/prepublished>  } } WHERE { graph <http://metadata.lincsproject.ca> { <" + GRAPH + "> <http://id.lincsproject.ca/status> <http://id.lincsproject.ca/published>, <http://id.lincsproject.ca/prepublished> } }")    
        print("\nDROPING STATUS FROM METADATA: ", parseResult(result))

def queryHelper(queryType, target, query):
    if queryType == 'SELECT':
        return queryService(targetLookup[target], JSON, GET, query)
    elif queryType == 'CONSTRUCT':
        return queryService(targetLookup[target], RDFXML, GET, query)
    elif queryType == 'UPDATE':
        return queryService(targetLookup[target], None, POST, query)
    
def queryService(target, format, action, query):
    sparql = SPARQLWrapper(target['ENDPOINT'])
    sparql.setCredentials(target['USER'], target['PW'])     
    if format:
        sparql.setReturnFormat(format)
    sparql.setMethod(action)
    sparql.setQuery(query)
    try:
        return sparql.queryAndConvert()
    except Exception as e:
        print(e)

def createUpdateQuery(data, graphName):
    # parses RDFXML into an UPDATE query
    updatequery = " ".join([f"PREFIX {prefix}: {ns.n3()}" for prefix, ns in data.namespaces()])
    updatequery += f" INSERT DATA {{ GRAPH <" + graphName + f"> {{"
    updatequery += " . ".join([f"{s.n3()} {p.n3()} {o.n3()}" for (s, p, o) in data.triples((None, None, None))])
    updatequery += f" . }}}}"
    return updatequery

def parseResult(result):
    match = re.compile('<p>(.*)</p').search(str(result))
    return match.group(1)


if __name__ == '__main__':
    main()