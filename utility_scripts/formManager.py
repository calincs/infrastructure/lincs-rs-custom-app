import json
from SPARQLWrapper import SPARQLWrapper, JSON
import re
import collections
import requests
from rdflib import Dataset, URIRef, BNode, Literal, XSD

# query the triplestore for a list of rdf types
# compare the form_templates_manifest.json records with the database results
# create new records for any missing rdf types that have since been added
# list the new records added, and add them to a new file output

def main():
    #updateTypes()   
    #updateProperties()
    #updateForms()
    updateConfig()
    ## warning: if you re-run updateProperties, 
    # its not going to update the modal component with the correct filename
    ## also, make sure you've removed the leading 22 from 22-rdf-syntax-ns_value when it is used as a property (OA uses it)


def updateConfig():
    """connect forms to their respective class. generates an authority manager config type map for upload to RS"""

    manifest = json.load(open("form_templates_manifest.json", "r"))  
    type_configurations_base_domain = "http://www.researchspace.org/resource/system/vocab/authority_manager_config_types/"
    prefixes = {    "http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work": "frbroo",
    "http://purl.org/dc/terms/": "dcterms",
    "http://purl.org/spar/cito/": "cito",
    "http://www.cidoc-crm.org/cidoc-crm/": "crm",
    "http://www.ics.forth.gr/isl/CRMdig/": "crmdig",
    "http://www.researchspace.org/ontology/": "rso",
    "http://www.w3.org/1999/02/22-rdf-syntax-ns#": "rdf",
    "http://www.w3.org/2000/01/rdf-schema#": "rdfs",
    "http://www.w3.org/2002/07/owl#": "owl",
    "http://www.w3.org/2004/02/skos/core#": "skos",
    "http://www.w3.org/ns/activitystreams#": "as",
    "http://www.w3.org/ns/oa#": "oa",
    "http://xmlns.com/foaf/0.1/": "foaf"
}

    lines = [] 

    for key in manifest.keys():
        # Building Type Configuration
        print(key)
        for prefix in prefixes.keys():
            if prefix in manifest[key]['iri']:
                labelx = "{} ({}:{})".format(manifest[key]['name'], prefixes[prefix], manifest[key]['name'])
        data = getData("SELECT ?description where {<" + manifest[key]['iri'] + "> <http://www.w3.org/2000/01/rdf-schema#comment>|<http://www.w3.org/2004/02/skos/core#definition> ?description}")
        if len(data)>0:
            description = data[0]['description']['value']
        else:
            description = None
        lines.append("<{}{}> <http://www.cidoc-crm.org/cidoc-crm/P71i_is_listed_in> <http://www.researchspace.org/resource/system/vocab/authority_manager_config_types> .".format(type_configurations_base_domain, manifest[key]['name']))
        lines.append("<{}{}> <http://www.w3.org/2000/01/rdf-schema#label> \"Authority Config for {}\"^^xsd:string .".format(type_configurations_base_domain, manifest[key]['name'], manifest[key]['name']))
        if description:
            lines.append("<{}{}> <http://www.w3.org/2000/01/rdf-schema#comment> \"\"\"{}\"\"\"^^xsd:string .".format(type_configurations_base_domain, manifest[key]['name'], description))
        else:
            print('no description')
        lines.append("<{}{}> <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> <http://www.researchspace.org/resource/system/FormConfig> .".format(type_configurations_base_domain, manifest[key]['name']))
        lines.append("<{}{}> <http://www.researchspace.org/resource/system/authority_manager/item_label> \"{}\"^^xsd:string .".format(type_configurations_base_domain, manifest[key]['name'], labelx))
        lines.append("<{}{}> <http://www.researchspace.org/resource/system/authority_manager/for_type> <{}>	.".format(type_configurations_base_domain, manifest[key]['name'], manifest[key]['iri']))
        lines.append("<{}{}> <http://www.researchspace.org/resource/system/authority_manager/uses_form> <http://www.researchspace.org/resource/forms/authoring/{}> .".format(type_configurations_base_domain, manifest[key]['name'], manifest[key]['name']))
        
    with open('http%3A%2F%2Fwww.researchspace.org%2Fresource%2Fsystem%2Fvocab%2Fauthority_manager_config_types.ttl', "w+") as f:
        for line in lines:
            f.write("{}\n".format(line))
    
    print('generated new type config map at http%3A%2F%2Fwww.researchspace.org%2Fresource%2Fsystem%2Fvocab%2Fauthority_manager_config_types')

def updateForms():
    """converts the form templates manifest into new forms"""
    stub1 = open("form_templates/stub1.txt", "r").read()
    stub2 = open("form_templates/stub2.txt", "r").read()
    stub3 = open("form_templates/stub3.txt", "r").read()
    stub1_modal = open("form_templates/stub1_modal.txt", "r").read()
    stub2_modal = open("form_templates/stub2_modal.txt", "r").read()
    stub3_modal = open("form_templates/stub3_modal.txt", "r").read()
    authorGeneric = open("form_templates/authorFormGeneric.txt", "r").read()
    authorPerson = open("form_templates/authorFormPerson.txt", "r").read()
    authorPlace = open("form_templates/authorFormPlace.txt", "r").read()
    sort_list = open("property_sort.txt", "r").readlines()
    sort_list = [item.strip("\n") for item in sort_list]
    prefixes = {    "http://iflastandards.info/ns/fr/frbr/frbroo/F1_Work": "frbroo",
                    "http://purl.org/dc/terms/": "dcterms",
                    "http://purl.org/spar/cito/": "cito",
                    "http://www.cidoc-crm.org/cidoc-crm/": "crm",
                    "http://www.ics.forth.gr/isl/CRMdig/": "crmdig",
                    "http://www.researchspace.org/ontology/": "rso",
                    "http://www.w3.org/1999/02/22-rdf-syntax-ns#": "rdf",
                    "http://www.w3.org/2000/01/rdf-schema#": "rdfs",
                    "http://www.w3.org/2002/07/owl#": "owl",
                    "http://www.w3.org/2004/02/skos/core#": "skos",
                    "http://www.w3.org/ns/activitystreams#": "as",
                    "http://www.w3.org/ns/oa#": "oa",
                    "http://xmlns.com/foaf/0.1/": "foaf"
                }
    
    manifest = json.load(open("form_templates_manifest.json", "r"))
    for key in manifest.keys():
        if manifest[key]["create_form"]:
            print(key)
            record = manifest[key]
            record['fieldDefinitions'] = dict(collections.OrderedDict(sorted(record['fieldDefinitions'].items(), key=lambda pair: sort_list.index(pair[0]))))
            
            # Building Form Template
            fieldDefs = str()
            modal_components = str()
            components = str()
            output = str()
            modal_output = str()
            fn = record['name'].replace(' ', '_').replace(')', '').replace(':','_')
            filename = "new_forms/http%3A%2F%2Fwww.researchspace.org%2Fresource%2Fforms%2Fauthoring%2F" + fn + ".html"
            modal_filename = "new_forms/http%3A%2F%2Fwww.researchspace.org%2Fresource%2Fforms%2Fmodal%2F" + fn + ".html"
            class_name = manifest[key]['name']
            

            for key in record['fieldDefinitions'].keys():           
                try:
                    field = record['fieldDefinitions'][key]
                    fieldDefs = fieldDefs + "{} = \"{}\"\n                    ".format(field['name'], field['iri'])
                    components = components + field['component'] + "\n        "
                    modal_components = modal_components + field['modal_component'] + "\n        "
                except Exception as e:
                    print(e)
                    pass
            
            if record['iri'] == "http://www.cidoc-crm.org/cidoc-crm/E53_Place":
                # lookup should use a place authority (currently the query is wikidata, like the rest)
                authorTemplate = authorPlace
            elif record['iri'] == "http://www.cidoc-crm.org/cidoc-crm/E21_Person":
                # lookup should use a person authority (currently the query is wikidata, like the rest)
                authorTemplate = authorPerson
            else:
                authorTemplate = authorGeneric.replace('CLASS_VAR', record['iri'])
            
            for prefix in prefixes.keys():
                if prefix in record['iri']:
                    label = "{} ({}:{})".format(record['name'], prefixes[prefix], record['name'])
            
            authorTemplate = authorTemplate.replace('LABEL_VAR', label)
            
            authorTemplate = authorTemplate.replace('DESCRIPTION_VAR', "Create a new {} entity from an external source".format(record['name']))
                        
            output = authorTemplate + fieldDefs +  stub1.replace("CLASS_VAR", record['iri']) + components.replace('LABEL_VAR', class_name) + stub2
            
            modal_output = stub1_modal.replace('TITLE_VAR', record['name']).replace('CLASS_VAR', record['iri']) + fieldDefs + stub2_modal + modal_components.replace('LABEL_VAR', class_name) + stub3_modal

            with open(filename, "w+") as f:
                f.write(output)
            with open(modal_filename, "w+") as f:
                f.write(modal_output)

def updateTypes():
    """get a fresh pull of types/classes from the database. creates a manifest consisting of empty records"""

    manifest = json.load(open("form_templates_manifest.json", "r"))
    types = getData("""SELECT DISTINCT ?type WHERE { 
            GRAPH ?src { ?s a ?type } .
            FILTER(strstarts(str(?src), "http://graph.lincsproject.ca"))
            }""")
    types_in_manifest = list(manifest.keys())
    types_in_database = [type['type']['value'] for type in types] 
    new_types = list(set(types_in_database) - set(types_in_manifest))
    
    print('The following new types are being added to the manifest:')
    for type in new_types:
        manifest[type] = {"name": re.sub(r"^.+/", "", type).replace("#", ""), "iri": type, "fieldDefinitions": {}}

    with open('form_templates_manifest_new.json', "w+") as f:
        json.dump(manifest, f)

def updateProperties():
    """ update the field definitions and components in the manifest based on contents of database"""
    
    # do not require graph permissions
    permissions = ["http://www.w3.org/2004/02/skos/core#ConceptScheme", "http://www.w3.org/2004/02/skos/core#Concept"]
    # do not need forms
    exclusions = ["http://rdfs.org/ns/void#Dataset", "http://purl.org/vocommons/voaf#Vocabulary"]

    manifest = json.load(open("form_templates_manifest.json", "r"))  


    for key in manifest:
        manifest[key]['fieldDefinitions'] = {}

    ### TO DO: we need to add modal form for both same_as and is_about. the modal form needs to be like entity editor switch form
    universal_fields =  {
        "label": {
            "iri": "http://www.w3.org/2000/01/rdf-schema#label",
            "name": "label",
            "component": "<semantic-form-text-input for=\"label\"></semantic-form-text-input>",
            "modal_component": "<semantic-form-text-input for=\"label\"></semantic-form-text-input>"
            },
        "P2_has_type_auto": {
            "iri": "http://www.cidoc-crm.org/cidoc-crm/P2_has_type",
            "name": "P2_has_type_auto",
            "component": "<semantic-form-autocomplete-input for=\"P2_has_type_auto\" label=\"has type (full-text lookup)\">[[> forms/modal/E55_Type]]</semantic-form-autocomplete-input>",
            "modal_component": "<semantic-form-autocomplete-input for=\"P2_has_type_auto\" label=\"has type (full-text lookup)\"></semantic-form-autocomplete-input>"
            },
        "P2_has_type_tree": {
            "iri": "http://www.cidoc-crm.org/cidoc-crm/P2_has_type",
            "name": "P2_has_type_tree",
            "component": "<semantic-form-tree-picker-input for=\"P2_has_type_tree\" label=\"has type (vocabulary tree)\">[[> forms/modal/E55_Type]]</semantic-form-tree-picker-input>",
            "modal_component": "<semantic-form-tree-picker-input for=\"P2_has_type_tree\" label=\"has type (vocabulary tree)\"></semantic-form-tree-picker-input>"
        },
        "same_as_auto": {
            "iri": "http://www.w3.org/2002/07/owl#sameAs",
            "name": "same_as_auto",
            "component": """<semantic-form-autocomplete-input for=\"same_as_auto\" label=\"same as (lookup)\"></semantic-form-autocomplete-input>
            <div class=\"create_new\"><semantic-link uri=\"http://www.researchspace.org/resource/ThinkingFrames?view=entity-editor\"><button type=\"button\" class=\"semantic-form-tree-picker-input__create-button btn btn-default\"><span class=\"fa fa-plus btn-icon-left\"></span><span>Create new</span></button></semantic-link></div>""",
            "modal_component": """<semantic-form-autocomplete-input for=\"same_as_auto\" label=\"same as (lookup)\"></semantic-form-autocomplete-input>
            <div class=\"create_new\">
                <semantic-link uri=\"http://www.researchspace.org/resource/ThinkingFrames?view=entity-editor\">
                    <button type=\"button\" class=\"semantic-form-tree-picker-input__create-button btn btn-default\">
                        <span class=\"fa fa-plus btn-icon-left\"></span><span>Create new</span>
                    </button>
                </semantic-link>
            </div>"""
        },
        "same_as_man": {
            "iri": "http://www.w3.org/2002/07/owl#sameAs",
            "name": "same_as_man",
            "component": "<semantic-form-text-input for=\"same_as_man\" label=\"same as (manual input)\"></semantic-form-text-input>",
            "modal_component": "<semantic-form-text-input for=\"same_as_man\" label=\"same as (manual input)\"></semantic-form-text-input>"
        },
        "P129_is_about": {
            "iri": "http://www.cidoc-crm.org/cidoc-crm/P129_is_about",
            "name": "P129_is_about",
            "component": """<semantic-form-autocomplete-input for=\"P129_is_about\"></semantic-form-autocomplete-input>
            <div class=\"create_new\"><semantic-link uri=\"http://www.researchspace.org/resource/ThinkingFrames?view=entity-editor\"><button type=\"button\" class=\"semantic-form-tree-picker-input__create-button btn btn-default\"><span class=\"fa fa-plus btn-icon-left\"></span><span>Create new</span></button></semantic-link></div><h2>LABEL_VAR Properties</h2>""",
            "modal_component": """<semantic-form-autocomplete-input for=\"P129_is_about\"></semantic-form-autocomplete-input>
            <div class=\"create_new\">
                <semantic-link uri=\"http://www.researchspace.org/resource/ThinkingFrames?view=entity-editor\">
                    <button type=\"button\" class=\"semantic-form-tree-picker-input__create-button btn btn-default\">
                        <span class=\"fa fa-plus btn-icon-left\"></span><span>Create new</span>
                    </button>
                </semantic-link>
            </div>
            <h2>LABEL_VAR Properties</h2>"""

        }
    }
    
    
    # add universal fields to the field definitions in manifest
    for key in manifest:
        manifest[key]['fieldDefinitions'].update(universal_fields)
        classtype = {"iri": "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
         "name": "classtype",
         "component": "<semantic-form-hidden-input for=\"classtype\" default-value=\"{}\"></semantic-form-hidden-input>".format(manifest[key]['iri']),
         "modal_component": "<semantic-form-hidden-input for=\"classtype\" default-value=\"{}\"></semantic-form-hidden-input>".format(manifest[key]['iri'])
        }
        manifest[key]['fieldDefinitions']['classtype'] = {}
        manifest[key]['fieldDefinitions']['classtype'].update(classtype)
        if key in permissions:
            manifest[key]['permission_required'] = False
        else:
            manifest[key]['permission_required'] = True
        if key in exclusions:
            manifest[key]['create_form'] = False
            manifest[key]['permission_required'] = False
        else:
            manifest[key]['create_form'] = True


    # excluded properties are those which we already have defined in the manifest.
    # when we are done this work and we want to update the manifest, we should turn this into an "included" list
    excluded_props = [
        "http://www.w3.org/1999/02/22-rdf-syntax-ns#type",
        "http://www.cidoc-crm.org/cidoc-crm/P2_has_type",
        "http://www.w3.org/2002/07/owl#sameAs",
        "http://www.cidoc-crm.org/cidoc-crm/P129_is_about",
        "http://www.w3.org/2000/01/rdf-schema#label"
    ]

    # fetch classes and properties from stage
    data = getData("""SELECT DISTINCT ?type ?property WHERE { 
            GRAPH ?src { ?s a ?type .
            ?s ?property ?o . }
            FILTER(strstarts(str(?src), "http://graph.lincsproject.ca"))}""")
    
    # a dictionary connecting the datatype of a KP to the type of form component
    componentType = {
        "http://www.w3.org/2001/XMLSchema#anyURI": "semantic-form-autocomplete-input",
        "http://www.w3.org/2001/XMLSchema#string": "semantic-form-text-input",
        "http://www.w3.org/2001/XMLSchema#langString": "semantic-form-text-input",
        "http://www.w3.org/2001/XMLSchema#date": "semantic-form-datetime-input",
        "http://www.w3.org/2001/XMLSchema#dateTime": "semantic-form-datetime-input"
    }
    
    errors = []
    for record in data:
        # iterating over each type/property
        if record['property']['value'] not in excluded_props:
            name = re.search(r".+\/(.+)", record['property']['value']).group(1).replace("#", "_")
            
            # get the datatype and range of this property from its KP
            dtypes = getData("select ?dataType where { <" + record['property']['value'] + "> <http://www.researchspace.org/resource/system/fields/xsdDatatype> ?dataType}")
            ranges = getData("select ?range where { <" + record['property']['value'] + "> <http://www.researchspace.org/resource/system/fields/range> ?range }")
                       
            #TO DO:
            # DO NOT ADD THE MODAL COMPONENT IF THE RANGE DOES NOT EXIST AS A CLASS.
            # WE WILL NEED TO EITHER CREATE A NEW FORM FOR THESE, OR FIND A NEW WAY OF ASSIGNING THE MODAL. POSSIBLY THE GENERIC CREATE.

            if (len(ranges) > 0) and (ranges[0]['range']['value'] not in ['http://www.cidoc-crm.org/cidoc-crm/E1_CRM_Entity']) and (ranges[0]['range']['value'] in manifest.keys()):
                # this property has a range property, and that range property is not E1_Entity

                manifest[record['type']['value']]['fieldDefinitions'][name] = {}
                manifest[record['type']['value']]['fieldDefinitions'][name].update({
                    "iri": record['property']['value'],
                    "name": name,
                    "component": "<{} for=\"{}\">[[> forms/modal/{} ]]</{}>".format(componentType[dtypes[0]['dataType']['value']], name, re.search(r".+\/(.+)", ranges[0]['range']['value']).group(1).replace("#", "_"), componentType[dtypes[0]['dataType']['value']]),
                    "modal_component": "<{} for=\"{}\"></{}>".format(componentType[dtypes[0]['dataType']['value']], name, componentType[dtypes[0]['dataType']['value']])
                })
            elif (len(ranges) > 0) and (ranges[0]['range']['value'] == 'http://www.cidoc-crm.org/cidoc-crm/E1_CRM_Entity'):
                # this property has a range property that is equal to E1_Entity
                manifest[record['type']['value']]['fieldDefinitions'][name] = {}
                manifest[record['type']['value']]['fieldDefinitions'][name].update({
                    "iri": record['property']['value'],
                    "name": name,
                    "component": "<{} for=\"{}\"></{}><div class=\"create_new\"><semantic-link uri=\"http://www.researchspace.org/resource/ThinkingFrames?view=entity-editor\"><button type=\"button\" class=\"semantic-form-tree-picker-input__create-button btn btn-default\"><span class=\"fa fa-plus btn-icon-left\"></span><span>Create new</span></button></semantic-link></div>".format(componentType[dtypes[0]['dataType']['value']], name, componentType[dtypes[0]['dataType']['value']]),
                    "modal_component": "<{} for=\"{}\"></{}><div class=\"create_new\"><semantic-link uri=\"http://www.researchspace.org/resource/ThinkingFrames?view=entity-editor\"><button type=\"button\" class=\"semantic-form-tree-picker-input__create-button btn btn-default\"><span class=\"fa fa-plus btn-icon-left\"></span><span>Create new</span></button></semantic-link></div>".format(componentType[dtypes[0]['dataType']['value']], name, componentType[dtypes[0]['dataType']['value']])
                })
            elif len(dtypes)>0:
                try:
                    manifest[record['type']['value']]['fieldDefinitions'][name] = {}
                    manifest[record['type']['value']]['fieldDefinitions'][name].update({
                        "iri": record['property']['value'],
                        "name": name,
                        "component": "<{} for=\"{}\"></{}>".format(componentType[dtypes[0]['dataType']['value']], name, componentType[dtypes[0]['dataType']['value']]),
                        "modal_component": "<{} for=\"{}\"></{}>".format(componentType[dtypes[0]['dataType']['value']], name, componentType[dtypes[0]['dataType']['value']])
                    })
                except Exception as e:
                    print(e)
                    pass
            else:
                errors.append(record['property']['value'])
    
    print('The following properties were either misspelled or missing a field definition:')
    for error in list(set(errors)):
        print(error)
    
    with open('form_templates_manifest_new.json', "w+") as f:
       json.dump(manifest, f)    

def getData(query):
    sparql = SPARQLWrapper("https://bg-review.lincsproject.ca/sparql")
    sparql.setCredentials(user="lincs", passwd="")
    sparql.setReturnFormat(JSON)
    sparql.setQuery(query)

    try:
        ret = sparql.queryAndConvert()
        return ret["results"]["bindings"]
    except Exception as e:
        print(e)


if __name__ == '__main__':
    main()