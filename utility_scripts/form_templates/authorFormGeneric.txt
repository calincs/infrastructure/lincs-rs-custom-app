<author-form external-fields='[
          {
            "id": "classtype",
            "label": "Class",
            "description": "The classtype",
            "xsdDatatype": "xsd:anyURI",
            "minOccurs": "1",
            "maxOccurs": "1",
            "selectPattern": "SELECT ?value WHERE {?subject a <CLASS_VAR>}",
            "insertPattern": "",
            "deletePattern": ""
          },
          {
            "id": "lookup",
            "label": "LABEL_VAR",
            "description": "DESCRIPTION_VAR",
            "xsdDatatype": "xsd:anyURI",
            "minOccurs": "1",
            "maxOccurs": "1",
            "selectPattern": "SELECT ?value WHERE {?subject a <CLASS_VAR>}",
            "autosuggestionPattern": "PREFIX wd: <http://www.researchspace.org/resource/system/services/wikidata/> PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX bds: <http://www.bigdata.com/rdf/search#> PREFIX skos: <http://www.w3.org/2004/02/skos/core#> PREFIX luc: <http://www.ontotext.com/owlim/lucene#> PREFIX aat: <http://vocab.getty.edu/aat/> PREFIX gvp: <http://vocab.getty.edu/ontology#> PREFIX xl: <http://www.w3.org/2008/05/skos-xl#> PREFIX dct: <http://purl.org/dc/terms/> PREFIX gvp_lang: <http://vocab.getty.edu/language/> PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/> SELECT ?label ?value WHERE { { SERVICE <https://rs.lincsproject.ca/sparql> { SELECT ?resource ?label ?sort where { GRAPH ?src { ?resource a <CLASS_VAR>; rdfs:label ?l . FILTER(lang(?l) NOT IN (\"el\", \"de\", \"zh\", \"fr\", \"pt\", \"ru\")) OPTIONAL { ?resource rdfs:comment ?description . FILTER(lang(?description) NOT IN (\"el\", \"de\", \"zh\", \"fr\", \"pt\", \"ru\")) } } SERVICE bds:search { ?l bds:search ?__token__; bds:minRelevance \"0.5\"; bds:matchAllTerms \"false\"; bds:relevance ?score. } BIND(\"0\" as ?sort) BIND( CONCAT(CONCAT(\"LINCS (<\", str(?src)), \">))\") as ?provider) BIND( IF(BOUND(?description), CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(str(?l), \" (IRI: <\"), str(?resource)), \"> Description: \"), ?description), \") (Source: \"), ?provider), CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(str(?l), \" (IRI: <\", str(?resource)), \">) (Source: \"), ?provider))) ) as ?label) } LIMIT 10 } } UNION { SERVICE <http://vocab.getty.edu/sparql> { select ?resource ?label WHERE { ?resource a skos:Concept; luc:term ?__token__; skos:inScheme aat: ; gvp:prefLabelGVP [xl:literalForm ?Term] optional {?resource skos:scopeNote [dct:language gvp_lang:en; rdf:value ?description]} BIND(?Term as ?l) BIND(IF(BOUND(?description), CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(?l, \" (IRI: <\"), str(?resource)), \"> Description: \"), ?description), \") (Source: AAT (Getty))\"), CONCAT(?l, \" (IRI: <\", str(?resource), \"> (Source: AAT (Getty))\") ) as ?label) } LIMIT 10 } } UNION { SERVICE wd:wikidataSearchService { SELECT * WHERE { ?subject wd:search ?__token__ ; wd:concepturi ?resource ; wd:label ?l ; wd:description ?description . } LIMIT 10 } BIND(IF(BOUND(?description), CONCAT(CONCAT(CONCAT(CONCAT(str(?l), \" (IRI: <\"), str(?resource), \"> Description: \"), ?description), \") (Source: Wikidata)\"), CONCAT(CONCAT(CONCAT(str(?l), \" (IRI: <\"), str(?resource)), \"> (Source: Wikidata)\") ) as ?label) } BIND(?resource as ?value) } ORDER BY DESC(?sort)",
            "insertPattern": "",
            "deletePattern": ""
          }]'
          node='{{node}}' template='{{> form}}'
          external-insert-query="PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                        PREFIX wd: <http://www.wikidata.org/entity/>
                        PREFIX dct: <http://purl.org/dc/terms/>
                        PREFIX gvp: <http://vocab.getty.edu/ontology#>
                        PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                        PREFIX gvp_lang: <http://vocab.getty.edu/language/>
                        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                        PREFIX xl: <http://www.w3.org/2008/05/skos-xl#>
                        INSERT { 
                          GRAPH <{graphIRI}> { 
                          <{iri}> a <CLASS_VAR> ;
                            rdfs:label ?label ;
                            rdfs:comment ?description .
                          }
                        } WHERE { 
                          { 
                              SERVICE <https://rs.lincsproject.ca/sparql> { 
                                  SELECT ?label ?description where { 
                                      <{iri}> rdfs:label ?label .
                                      OPTIONAL {<{iri}> rdfs:comment ?description}
                                      filter(lang(?label)='en')
                                  }
                              }
                          } UNION
                          { 
                              SERVICE <http://vocab.getty.edu/sparql> { 
                                  SELECT ?label ?description where { 
                                     <{iri}> gvp:prefLabelGVP [xl:literalForm ?label] .
                                    optional { <{iri}> skos:scopeNote [dct:language gvp_lang:en; rdf:value ?description] }
                                  }
                              }
                          } UNION
                          { 
                              SERVICE <https://query.wikidata.org/sparql> { 
                                  SELECT ?label ?description where { 
                                      <{iri}> rdfs:label ?label .
                                      optional {<{iri}> <http://schema.org/description> ?description .
                                      FILTER(lang(?description)='en') }
                                      filter(lang(?label)='en')
                                  }
                              }
                          }
                        }"
          frame='{{viewId}}'
          tokenizeLuceneQuery=false 
          escapeLuceneSyntax=false
          semantic-title='LABEL_VAR'
          semantic-title-iri='CLASS_VAR'
          semantic-fields='[[fieldDefinitions
                    