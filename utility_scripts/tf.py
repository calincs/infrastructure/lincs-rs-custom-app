from bs4 import BeautifulSoup
import os


directory = os.fsencode("../lincs/data/templates")
    
lookup = {
  "admin-panel": "http://www.researchspace.org/resource/admin",
  "home": "http://www.researchspace.org/resource/home",
  "datasets": "http://www.researchspace.org/resource/datasets",
  "dashboard": "http://www.researchspace.org/resource/ProjectDashboard",
  "knowledge-map-dashboard": "http://www.researchspace.org/resource/ProjectKnowledgeMaps",
  "semantic-narrative-dashboard": "http://www.researchspace.org/resource/ProjectNarratives",
  "feedback": "http://www.researchspace.org/resource/feedback",
  "search-all": "http://www.researchspace.org/resource/search/searchAll",
  "search-yellowNineties": "http://www.researchspace.org/resource/search/yellowNineties",
  "search-adarchive": "http://www.researchspace.org/resource/search/adarchive",
  "search-ethnomusicology": "http://www.researchspace.org/resource/search/ethnomusicology",
  "search-orlando": "http://www.researchspace.org/resource/search/orlando",
  "search-heresies": "http://www.researchspace.org/resource/search/heresies",
  "search-moeml": "http://www.researchspace.org/resource/search/moeml",
  "search-usaskart": "http://www.researchspace.org/resource/search/usaskart",
  "search-histSex": "http://www.researchspace.org/resource/search/histSex",
  "search-anthologiaGraeca": "http://www.researchspace.org/resource/search/anthologiaGraeca",
  "entity-editor": "http://www.researchspace.org/resource/EntityEditor",
  "permissions-editor": "http://www.researchspace.org/resource/forms/admin/permissions",
  "permissions-search": "http://www.researchspace.org/resource/search/permissions",
  "authority-config": "http://www.researchspace.org/resource/AuthorityAndEntityConfiguration",
  "authority-content": "http://www.researchspace.org/resource/AuthorityContent",
  "vocabulary-manager": "http://www.researchspace.org/resource/VocabularyManager",
  "knowledge-map": "http://www.researchspace.org/resource/ThinkingFramesKnowledgeMapTemplate",
  "semantic-narrative": "http://www.researchspace.org/resource/ThinkingFramesSemanticNarrativeTemplate",
  "sparql": "http://www.researchspace.org/resource/SPARQL",
  "system-narrative": "http://www.researchspace.org/resource/SystemNarrativesFrame",
  "system-knowledge-maps": "http://www.researchspace.org/resource/SystemKnowledgeMapsFrame",
  "system-assets": "http://www.researchspace.org/resource/SystemAssetsFrame",
  "system-activity": "http://www.researchspace.org/resource/SystemActivityFrame",
  "system-projects": "http://www.researchspace.org/resource/SystemProjects",
  "mapEntitySearch": "http://www.researchspace.org/resource/MapEntitySearch",
  "metadata-viewer": "http://www.researchspace.org/resource/search/metadata",
  "people": "http://www.researchspace.org/resource/search/people",
  "place": "http://www.researchspace.org/resource/search/place",
  "groups": "http://www.researchspace.org/resource/search/groups",
  "sources": "http://www.researchspace.org/resource/search/sources",
  "objects": "http://www.researchspace.org/resource/search/objects",
  "event": "http://www.researchspace.org/resource/search/event"
}


for filename in os.listdir(directory):
    
    with open(os.path.join(directory, filename)) as fp:
        soup = BeautifulSoup(fp, 'html.parser')
        
        
        for tag in soup.findAll("semantic-link-container"):
            if tag:
                if tag['uri'] == "[[resolvePrefix \"rsp:ThinkingFrames\"]]":
                    if tag['urlqueryparam-view'] == 'resource':
                        tag['uri'] = tag['urlqueryparam-resource']
                        del(tag['urlqueryparam-resource'])
                    else:
                        tag['uri'] = lookup[tag['urlqueryparam-view']]
                    del(tag["urlqueryparam-view"])

        for tag in soup.findAll("semantic-link"):
            
            if 'uri' in tag.attrs:
                if tag['uri'].replace('http://www.researchspace.org/resource/ThinkingFrames?view=', '') in lookup:
                    tag['uri'] = lookup[tag['uri'].replace('http://www.researchspace.org/resource/ThinkingFrames?view=', '')]
                elif tag['uri'] == "[[resolvePrefix \"rsp:ThinkingFrames\"]]":
                    if tag['urlqueryparam-view'] == 'resource':
                        tag['uri'] = tag['urlqueryparam-resource']
                        del(tag['urlqueryparam-resource'])
                    else:
                        tag['uri'] = lookup[tag['urlqueryparam-view']]
                    del(tag["urlqueryparam-view"])
            elif 'iri' in tag.attrs:
                if 'ThinkingFrames' in tag['iri']:
                    if tag['iri'].replace('http://www.researchspace.org/resource/ThinkingFrames?view=', '') in lookup:
                        tag['iri'] = lookup[tag['iri'].replace('http://www.researchspace.org/resource/ThinkingFrames?view=', '')]
                    else:
                        tag['iri'] = lookup[tag['urlqueryparam-view']]
                        del(tag["urlqueryparam-view"])


        with open("output/{}".format(str(filename).strip("b'").strip("'")), "w+") as save:
            save.write(str(soup))
            