import os
from rdflib.namespace import RDF, RDFS
from rdflib import Dataset, URIRef, BNode, Literal, XSD, Graph
import urllib.parse
import json
import csv
from datetime import datetime
import re

def main():
    #get all trig files in the ldp/assets folder
    #processor(getFiles())
   # ldpToManifest(getFiles())
    #manifestToLDP() 
    #jsonToCSV()
    #transformAll(getFiles())
    #extractCategories()
    #csvToLDP()
    #addFromTXT()
    
    for ldp in getFiles():
        g = Dataset()
        
        fileName = ldp.replace("../lincs/ldp/assets/","")
        subject = URIRef(urllib.parse.unquote(fileName).replace(".trig",""))
        context = URIRef(urllib.parse.unquote(fileName).replace(".trig","/context"))
        
        #open this ldp file
        try:
            g.parse(ldp, format='trig')
        except Exception as e:
            print(subject)
            print(e)
            print(context)
        
        
        if len(list(g.quads((subject, RDFS['comment'], None, context ))))>1:
            results = []
            for quad in g.quads((subject, URIRef('http://www.w3.org/2000/01/rdf-schema#comment'), None, context)):      
                results.append(quad[2].value)
            g.remove((subject, URIRef('http://www.w3.org/2000/01/rdf-schema#comment'), None, context))
            g.add((subject, URIRef('http://www.w3.org/2000/01/rdf-schema#comment'), Literal(' '.join(results)), context))
            g.serialize(destination="new_assets/{}".format(fileName), format='trig')
                                        
    #with open("out.csv", "w", newline="") as f:
    #    writer = csv.writer(f)
    #    writer.writerows(results)
                    
    
def addFromTXT():
    with open('ldp_fix.txt') as f:
        for line in f.readlines():
            line = line.strip().replace('\n',"")
            g = Dataset()
            fileName = urllib.parse.quote_plus(line) + ".trig"
            subject = URIRef(line)
            context = URIRef("{}/context".format(line.replace('\n',"")))
            try: 
                g.parse("../lincs/ldp/assets/{}".format(fileName), format='trig')
                g.add((subject, URIRef("http://www.researchspace.org/resource/system/fields/category"), URIRef("http://www.researchspace.org/resource/system/formContainer/category/default"), context))
                g.serialize(destination="new_assets/{}".format(fileName), format='trig')
            except Exception as e:
                print(e)
                pass


def extractCategories():   
    output = []
    manifest = json.load(open("ldp_manifest.json", "r"))
    categories = []
    for record in manifest:      
        if "http://www.researchspace.org/resource/system/fields/category" in record['data']:
            if isinstance(record['data']["http://www.researchspace.org/resource/system/fields/category"], list):
                for category in record['data']["http://www.researchspace.org/resource/system/fields/category"]:
                    categories.append(category['value'])
            else:
                categories.append(record['data']["http://www.researchspace.org/resource/system/fields/category"]['value'])
    
    for record in manifest:    
        print(record['subject'])
        print(record)
        print('')
        if "http://www.researchspace.org/resource/system/fields/category" in record['data']:
            if isinstance(record['data']["http://www.researchspace.org/resource/system/fields/category"], list):
                for category in record['data']["http://www.researchspace.org/resource/system/fields/category"]:
                    output.append( {"category": category['value'], "iri": record['subject'] , "select": record['data']['http://www.researchspace.org/resource/system/fields/selectPattern']['value'].replace('\n', '')})
            
            else:
                output.append( {"category": record['data']['http://www.researchspace.org/resource/system/fields/category']['value'], "iri": record['subject'] , "select": record['data']['http://www.researchspace.org/resource/system/fields/selectPattern']['value'].replace('\n', '')})
                
    with open("categories.csv", "w+") as f:
        writer = csv.DictWriter(f, fieldnames=['category','iri','select'])
        writer.writeheader()
        for row in output:
            writer.writerow(row)



def addFromCSV():
    """Add csv data to ldp file, row by row"""
    with open('kp_update.csv') as csvfile:
        mapping = {'label_en': ('http://www.w3.org/2000/01/rdf-schema#label', 'en'), 'label_fr': ('http://www.w3.org/2000/01/rdf-schema#label', 'fr'), "description_en": ('http://www.w3.org/2000/01/rdf-schema#comment', 'en'), 'description_fr': ('http://www.w3.org/2000/01/rdf-schema#comment', 'fr')}
        reader = csv.DictReader(csvfile)
        for row in reader:
            g = Dataset()
            fileName = urllib.parse.quote_plus(row['id']) + ".trig"
            context = URIRef("{}/context".format(row['id']))
            subject = URIRef(row['id'])
            g.parse("../lincs/ldp/assets/{}".format(fileName), format='trig')
            flag = 'n'
            for label in row:
                if (label!= 'id') and (row[label]!=''):
                    if row[label] not in [quad[2].value for quad in g.quads((subject, URIRef(mapping[label][0]), None, context))]:
                        flag = 'y'
                        g.add((subject, URIRef(mapping[label][0]), Literal(row[label], lang=mapping[label][1]), context))            
            if flag=='y':
                g.serialize(destination="new_assets/{}".format(fileName), format='trig')
    

def transformCSV(ldpFiles):
    
    with open('missingKPs.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            g = Dataset()
            fileName = urllib.parse.quote_plus(row['iri']) + ".trig"
            subject = URIRef(row['iri'])
            context = URIRef(row['iri'] + "/context")    

    #results = {}
    #predicates = {}
    for ldp in ldpFiles:    
        g = Dataset()
        fileName = ldp.replace("../lincs/ldp/assets/","")
        subject = URIRef(urllib.parse.unquote(fileName).replace(".trig",""))
        context = URIRef(urllib.parse.unquote(fileName).replace(".trig","/context"))
        
        #open this ldp file
        try:
            g.parse(ldp, format='trig')
        except Exception as e:
            print(e)
            print(context)

        #if len(list(g.quads((subject, RDF['type'], URIRef('http://www.researchspace.org/resource/system/fields/Field'), context ))))<1:
#            if subject not in ["http://researchspace.org/container/rootContainer/ontologyContainer","http://www.researchspace.org/resource/system/fieldDefinitionContainer","http://www.cidoc-crm.org/cidoc-crm/CRMba/","http://www.researchspace.org/ontology/","http://www.ics.forth.gr/isl/CRMdig/","http://www.cidoc-crm.org/cidoc-crm/","http://www.ics.forth.gr/isl/CRMsci/","http://www.cidoc-crm.org/cidoc-crm/CRMarchaeo/",
#            "http://iflastandards.info/ns/fr/frbr/frbroo/","http://www.ics.forth.gr/isl/CRMinf/","http://www.cidoc-crm.org/cidoc-crm/influence/","http://www.ics.forth.gr/isl/CRMgeo/"]:
        
        for quad in g.quads((subject, URIRef('http://www.researchspace.org/resource/system/fields/category'), None, context)):
            #if the graph contains a category tag, add the tag to the results dictionary and add the graph IRI
            if str(quad[2]) in results:
                results[str(quad[2])].append(str(subject))
            else:
                results[str(quad[2])] = [str(subject)]

        #extract the select statement from an LDP
        
        print(subject)
        for quad in g.quads((subject, URIRef('http://www.researchspace.org/resource/system/fields/selectPattern'), None, context)):
            for blank_node in g.quads((quad[2], URIRef('http://spinrdf.org/sp#text'), None, context)):
                
                
                m = re.search('<(.+?)>', statement)
                if m:
                    print(m.groups())
                print('')
                    #temp = m.group(1)
                    #if temp not in predicates:
                    #    predicates[temp] = [str(subject)]
                    #else:
                    #    predicates[temp].append(str(subject))

def transformAll(ldpFiles):
    #loop over all trig files in the ldp/assets folder (ignoring non IRI fields)
   # results = {}
    #predicates = {}
    for ldp in ldpFiles:    
        g = Dataset()
        fileName = ldp.replace("../lincs/ldp/assets/","")
        subject = URIRef(urllib.parse.unquote(fileName).replace(".trig",""))
        context = URIRef(urllib.parse.unquote(fileName).replace(".trig","/context"))
        
        #open this ldp file
        try:
            g.parse(ldp, format='trig')
        except Exception as e:
            print(e)
            print(context)

    #    if len(list(g.quads((subject, RDF['type'], URIRef('http://www.researchspace.org/resource/system/fields/Field'), context ))))<1:
    #        if subject not in ["http://researchspace.org/container/rootContainer/ontologyContainer","http://www.researchspace.org/resource/system/fieldDefinitionContainer","http://www.cidoc-crm.org/cidoc-crm/CRMba/","http://www.researchspace.org/ontology/","http://www.ics.forth.gr/isl/CRMdig/","http://www.cidoc-crm.org/cidoc-crm/","http://www.ics.forth.gr/isl/CRMsci/","http://www.cidoc-crm.org/cidoc-crm/CRMarchaeo/",
    #        "http://iflastandards.info/ns/fr/frbr/frbroo/","http://www.ics.forth.gr/isl/CRMinf/","http://www.cidoc-crm.org/cidoc-crm/influence/","http://www.ics.forth.gr/isl/CRMgeo/"]:
        
        for quad in g.quads((subject, URIRef('http://www.researchspace.org/resource/system/fields/category'), None, context)):
            if quad[2] == 'http://www.researchspace.org/resource/system/fields/category/actor':
                print(subject, quad)
            

        #extract the select statement from an LDP
        
 #       print(subject)
 #       for quad in g.quads((subject, URIRef('http://www.researchspace.org/resource/system/fields/selectPattern'), None, context)):
 #           for blank_node in g.quads((quad[2], URIRef('http://spinrdf.org/sp#text'), None, context)):
                
                
  #              m = re.search('<(.+?)>', statement)
  #              if m:
 #                   print(m.groups())
 #               print('')
  #                  #temp = m.group(1)
                    #if temp not in predicates:
                    #    predicates[temp] = [str(subject)]
                    #else:
                    #    predicates[temp].append(str(subject))

   # duplicates = [p for p in predicates if len(predicates[p])>1]
    #for d in duplicates:
        #print(results[d])
    #for r in results['http://www.researchspace.org/resource/system/formContainer/category/default']:
   #     print(r)
 #   for p in predicates:
  #      if len(predicates[p]) > 1:
#            print(p, predicates[p])
   # for predicate in predicates.keys():
    #    predicates[predicate] = set(predicates[predicate])
        #print(predicate, predicates[predicate])


    #for key in results.keys():
    #    print(key, results[key])

    #print(len(results['http://www.researchspace.org/resource/system/formContainer/category/default']))
    #print(len(ldpFiles))
            
                #g.serialize(destination="new_assets/{}".format(fileName), format='trig')
        

def jsonToCSV():    

    manifest = json.load(open("ldp_manifest.json", "r"))
    categories = ["http://www.researchspace.org/resource/system/formContainer/category/actor","http://www.researchspace.org/resource/system/formContainer/category/artworks","http://www.researchspace.org/resource/system/formContainer/category/adarchive","http://www.researchspace.org/resource/system/formContainer/category/forms_adarchive","http://www.researchspace.org/resource/system/formContainer/category/bibliographic","http://www.researchspace.org/resource/system/formContainer/category/event","http://www.researchspace.org/resource/system/formContainer/category/linguistic_object","http://www.researchspace.org/resource/system/formContainer/category/orlando_actor"]
    lookup = [this_line.strip('\n') for this_line in open('all_predicates.txt', 'r+').readlines()]
    output = []
    for record in manifest:
        if record['subject'] in lookup:
            if 'http://www.w3.org/2000/01/rdf-schema#comment' not in record['data'].keys():
                if isinstance(record['data']["http://www.w3.org/2000/01/rdf-schema#label"], list):
                    temp = {'id': record['subject'], 'selectStatement': record['data']['http://www.researchspace.org/resource/system/fields/selectPattern']['value'], 'type': 'standard'}
                    for label in record['data']["http://www.w3.org/2000/01/rdf-schema#label"]:
                        temp["label_" + label['lang']] = label['value']
                    output.append(temp)
                else:
                    if 'lang' in record['data']["http://www.w3.org/2000/01/rdf-schema#label"]:
                        output.append({'id': record['subject'], 'selectStatement': record['data']['http://www.researchspace.org/resource/system/fields/selectPattern']['value'], 'type': 'standard', 'label_{}'.format(record['data']["http://www.w3.org/2000/01/rdf-schema#label"]['lang']): record['data']["http://www.w3.org/2000/01/rdf-schema#label"]['value']})
                    else:
                        output.append({'id': record['subject'], 'selectStatement': record['data']['http://www.researchspace.org/resource/system/fields/selectPattern']['value'], 'type': 'standard', 'label_en': record['data']["http://www.w3.org/2000/01/rdf-schema#label"]['value']})
        else:
            if 'http://www.researchspace.org/resource/system/fields/selectPattern' in record['data']:
                for item in lookup:       
                    if (item in record['data']['http://www.researchspace.org/resource/system/fields/selectPattern']['value']) and ('http://www.w3.org/2000/01/rdf-schema#comment' not in record['data'].keys()):
                        if isinstance(record['data']["http://www.w3.org/2000/01/rdf-schema#label"], list):
                            temp = {'id': record['subject'], 'selectStatement': record['data']['http://www.researchspace.org/resource/system/fields/selectPattern']['value'], 'type': 'custom'}
                            for label in record['data']["http://www.w3.org/2000/01/rdf-schema#label"]:
                                temp["label_" + label['lang']] = label['value']
                            output.append(temp)
                        else:
                            if 'lang' in record['data']["http://www.w3.org/2000/01/rdf-schema#label"]:
                                output.append({'id': record['subject'], 'selectStatement': record['data']['http://www.researchspace.org/resource/system/fields/selectPattern']['value'], 'type': 'custom', 'label_{}'.format(record['data']["http://www.w3.org/2000/01/rdf-schema#label"]['lang']): record['data']["http://www.w3.org/2000/01/rdf-schema#label"]['value']})
                            else:
                                output.append({'id': record['subject'], 'selectStatement': record['data']['http://www.researchspace.org/resource/system/fields/selectPattern']['value'], 'type': 'custom', 'label_en': record['data']["http://www.w3.org/2000/01/rdf-schema#label"]['value']})
                                
    with open('ldp_to_csv.json', 'w+') as f:
        json.dump(output, f)

def csvToLDP():
    # open csv of KPS
    # for each KP, build a manifest entry and write it.
    # expects csv to contain fields: iri,label,datatype,domain,range,comment

    with open('missing_kps.csv', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            g = Dataset()
            fileName = urllib.parse.quote_plus(row['iri']) + ".trig"
            subject = URIRef(row['iri'])
            context = URIRef(row['iri'] + "/context")

            g.add((URIRef('http://www.researchspace.org/resource/system/fieldDefinitionContainer'), URIRef('http://www.w3.org/ns/ldp#contains'), subject, context))
            g.add((subject, RDF["type"], URIRef("http://www.researchspace.org/resource/system/fields/Field"), context))
            g.add((subject, RDF["type"], URIRef("http://www.w3.org/ns/ldp#Resource"), context))
            g.add((subject, RDF["type"], URIRef("http://www.w3.org/ns/prov#Entity"), context))
            g.add((subject, URIRef("http://www.w3.org/ns/prov#generatedAtTime"), Literal(datetime.now(), datatype=XSD.date), context))
            g.add((subject, URIRef("http://www.w3.org/ns/prov#wasAttributedTo"), URIRef("http://www.researchspace.org/resource/user/zschoenb"), context))
            g.add((subject, URIRef("http://www.w3.org/2000/01/rdf-schema#label"), Literal(row['label']), context))
            g.add((subject, URIRef("http://www.researchspace.org/resource/system/fields/xsdDatatype"), URIRef(row['datatype']), context))
            if row['range'] != '':
                g.add((subject, URIRef("http://www.researchspace.org/resource/system/fields/range"), URIRef(row['range']), context))
            if row['domain'] != '':
                g.add((subject, URIRef("http://www.researchspace.org/resource/system/fields/domain"), URIRef(row['domain']), context))

            for category in row['categories'].split(','):
                g.add((subject, URIRef("http://www.researchspace.org/resource/system/fields/category"), URIRef(category), context))

            node = BNode()
            g.add((subject, URIRef("http://www.researchspace.org/resource/system/fields/selectPattern"), node, context))
            g.add((node, RDF['type'], URIRef('http://spinrdf.org/sp#Query'), context))
            g.add((node, URIRef('http://spinrdf.org/sp#text'), Literal("SELECT $value WHERE { "+ row['query'] + " }"), context))

            node = BNode()
            g.add((subject, URIRef("http://www.researchspace.org/resource/system/fields/insertPattern"), node, context))
            g.add((node, RDF['type'], URIRef('http://spinrdf.org/sp#Query'), context))
            g.add((node, URIRef('http://spinrdf.org/sp#text'), Literal("INSERT { " + row['query'] + " } WHERE { }"), context))   

            node = BNode()
            g.add((subject, URIRef("http://www.researchspace.org/resource/system/fields/deletePattern"), node, context))
            g.add((node, RDF['type'], URIRef('http://spinrdf.org/sp#Query'), context))
            g.add((node, URIRef('http://spinrdf.org/sp#text'), Literal("DELETE { " + row['query'] + " } WHERE { }"), context))

            
            if row['datatype'] == 'http://www.w3.org/2001/XMLSchema#anyURI':
                node = BNode()
                pattern = """
                SELECT ?value ?label WHERE { 
                GRAPH ?graph { 
                    ?value rdf:type ?type  ;
                    rdfs:label ?valueLabel.
                } .
                SERVICE bds:search {
                    ?valueLabel bds:search ?__token__;
                    bds:minRelevance "0.3";
                    bds:matchAllTerms "false";
                    bds:relevance ?score.
                }
                    ?type (rdfs:subClassOf*) <""" + row['range'] + """> ;
                    rdfs:label ?typeLabel .
                    FILTER(STRSTARTS(STR(?graph), "http://graph.lincsproject.ca/")) .
                    FILTER(lang(?typeLabel)="en") .
                    BIND(CONCAT((CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(?valueLabel, " ("), str(?value)),") (a ", str(?type),") from ",str(?graph)))))) as ?label) .
                }
                    ORDER BY DESC(?score)
                    LIMIT 50
                """
                g.add((subject, URIRef("http://www.researchspace.org/resource/system/fields/autosuggestionPattern"), node, context))
                g.add((node, RDF['type'], URIRef('http://spinrdf.org/sp#Query'), context))
                g.add((node, URIRef('http://spinrdf.org/sp#text'), Literal(pattern), context))
            
            g.serialize(destination="new_assets/{}".format(fileName), format='trig')


def manifestToLDP():
    manifest = json.load(open("ldp_manifest.json", "r"))
    for record in manifest:
        if record['needs_update']==True:
            g = Dataset()
            subject = URIRef(record['subject'])
            context = URIRef(record['context'])
            for predicate in record['data'].keys():
                if predicate in ["http://www.researchspace.org/resource/system/fields/autosuggestionPattern","http://www.researchspace.org/resource/system/fields/deletePattern","http://www.researchspace.org/resource/system/fields/selectPattern","http://www.researchspace.org/resource/system/fields/insertPattern"]:
                    #create a bnode for this object
                    node = BNode()
                    g.add((subject, URIRef(predicate), node, context))
                    g.add((node, RDF['type'], URIRef('http://spinrdf.org/sp#Query'), context))
                    # the pattern will always be a literal
                    g.add((node, URIRef('http://spinrdf.org/sp#text'), Literal(record['data'][predicate]['value']), context))   
                elif isinstance(record['data'][predicate], list):
                    # a list of objects
                    for object in record['data'][predicate]:
                        if object['type']=='Literal':
                            g.add((subject, URIRef(predicate), Literal(object['value']), context))
                        elif object['type']=='URIRef':
                            g.add((subject, URIRef(predicate), URIRef(object['value']), context))
                else:
                    if record['data'][predicate]['type']=='Literal':
                        g.add((subject, URIRef(predicate), Literal(record['data'][predicate]['value']), context))
                    elif record['data'][predicate]['type']=='URIRef':
                        g.add((subject, URIRef(predicate), URIRef(record['data'][predicate]['value']), context))
            g.add((URIRef('http://www.researchspace.org/resource/system/fieldDefinitionContainer'), URIRef('http://www.w3.org/ns/ldp#contains'), subject, context))
            g.serialize(destination="new_assets/{}".format(record['filename']), format='trig')

def ldpToManifest(ldpFiles):
    """pulls LDP files and compiles their contents into a JSON file"""
    manifest = []
    for ldp in ldpFiles:    
        g = Dataset()
        fileName = ldp.replace("../lincs/ldp/assets/","")
        subject = URIRef(urllib.parse.unquote(fileName).replace(".trig",""))
        context = URIRef(urllib.parse.unquote(fileName).replace(".trig","/context"))  
        print(fileName)
        g.parse(ldp, format='trig')
        # SKIP ONTOLOGY FILES! We only want field definitions
        if len(list(g.quads((subject, RDF['type'], URIRef('http://www.researchspace.org/resource/system/fields/Field'), context ))))>0:
            record = {"subject": subject, "context": context, "filename": fileName, "exists_as_ldp": True, "needs_update": False, "data": {} }

            for quad in g.quads((subject, None, None, context )):
                
                
                if isinstance(quad[2], BNode):
                    for blank_node in g.quads((quad[2], URIRef('http://spinrdf.org/sp#text'), None, context)):
                        if isinstance(quad[2], Literal) and (quad[2].language):
                            record['data'][quad[1]] = {"type": type(quad[2]).__name__, "value": blank_node[2], "lang": quad[2].language} 
                        else:
                            record['data'][quad[1]] = {"type": type(quad[2]).__name__, "value": blank_node[2]}
                elif len(list(g.quads((subject, quad[1], None, context))))<2:
                    # only one object exists for this predicate. so make it a string.
                    if isinstance(quad[2], Literal) and (quad[2].language):
                        record['data'][quad[1]] = {"type": type(quad[2]).__name__, "value": quad[2], "lang": quad[2].language}
                    else:
                        record['data'][quad[1]] = {"type": type(quad[2]).__name__, "value": quad[2]}
                elif quad[1] in record['data']:
                    #an entry has already been made in the record, and there are more than one objects for this predicate
                    if isinstance(quad[2], Literal) and (quad[2].language):
                        record['data'][quad[1]].append({"type": type(quad[2]).__name__, "value": quad[2], "lang": quad[2].language})
                    else:
                        record['data'][quad[1]].append({"type": type(quad[2]).__name__, "value": quad[2]})
                else:
                    #an entry needs to be made in the record, and there are more than one objects for this predicate
                    if isinstance(quad[2], Literal) and (quad[2].language):
                        record['data'][quad[1]] = [{"type": type(quad[2]).__name__, "value": quad[2], "lang": quad[2].language}]
                    else:
                        record['data'][quad[1]] = [{"type": type(quad[2]).__name__, "value": quad[2]}]
                    
            manifest.append(record)
        else:
            #print(subject)
            manifest.append(record)
    with open('ldp_manifest_new.json', 'w+') as f:
        json.dump(manifest, f)


def processor(ldpFiles):
    #loop over all trig files in the ldp/assets folder (ignoring non IRI fields)
    for ldp in ldpFiles:    
        g = Dataset()
        fileName = ldp.replace("../lincs/ldp/assets/","")
        print(fileName)
        subject = URIRef(urllib.parse.unquote(fileName).replace(".trig",""))
        context = URIRef(urllib.parse.unquote(fileName).replace(".trig","/context"))
        g.parse(ldp, format='trig')
        
        # SKIP ONTOLOGY FILES! We only want field definitions
        if len(list(g.quads((subject, RDF['type'], URIRef('http://www.researchspace.org/resource/system/fields/Field'), context ))))>0:
            addPatterns(g, subject, context, fileName)
            # today, we want to add an autosuggestion field to the LDP container
            # we only want to add this autosuggestion field if the datatype is IRI
            #if len(list(g.quads((subject, URIRef("http://www.researchspace.org/resource/system/fields/xsdDatatype"), URIRef("http://www.w3.org/2001/XMLSchema#anyURI"), context))))>0:
            #    g = addAutoSuggestion(g, subject, context, fileName)
        else:
            g.serialize(destination="new_assets/{}".format(fileName), format='trig')

def addPatterns(g, subject, context, fileName):
        selectPattern = Literal("SELECT ?value where { ?subject <" + subject + "> ?value }")
        insertPattern = Literal("INSERT { ?subject <" + subject + "> ?value } WHERE { }")
        deletePattern = Literal("DELETE { ?subject <" + subject + "> ?value } WHERE { }")
        
        
        g.add((URIRef('http://www.researchspace.org/resource/system/fieldDefinitionContainer'), URIRef('http://www.w3.org/ns/ldp#contains'), subject, context))

        if len(list(g.quads((subject, URIRef("http://www.researchspace.org/resource/system/fields/insertPattern"), None, context))))<1:
            createNode(g, subject, context, insertPattern, 'http://www.researchspace.org/resource/system/fields/insertPattern')
        if len(list(g.quads((subject, URIRef("http://www.researchspace.org/resource/system/fields/deletePattern"), None, context))))<1:
            createNode(g, subject, context, deletePattern, 'http://www.researchspace.org/resource/system/fields/deletePattern')
        if len(list(g.quads((subject, URIRef("http://www.researchspace.org/resource/system/fields/selectPattern"), None, context))))<1:
            createNode(g, subject, context, selectPattern, 'http://www.researchspace.org/resource/system/fields/selectPattern')
        g.serialize(destination="new_assets/{}".format(fileName), format='trig')
        return g


def createNode(g, subject, context, pattern, predicate):
    node = BNode()
    g.add((subject, URIRef(predicate), node, context))
    g.add((node, RDF['type'], URIRef('http://spinrdf.org/sp#Query'), context))
    g.add((node, URIRef('http://spinrdf.org/sp#text'), pattern, context))   

def addAutoSuggestion(g, subject, context, fileName):
    ranges = []
    
    # if an autosuggestion property already exists on this graph, remove it.
    g.remove((subject, URIRef('http://www.researchspace.org/resource/system/fields/autosuggestionPattern'), None, context))
    
    for statement in g.quads((subject, URIRef("http://www.researchspace.org/resource/system/fields/range"), None, context)):
        ranges.append("<{}>".format(statement[2]))
    
    if len(ranges) > 1:
        autoQuery = """SELECT ?value ?label WHERE { 
            GRAPH ?graph { 
                ?value rdf:type ?type  ;
                rdfs:label ?valueLabel.
            } .
            SERVICE bds:search {
                ?valueLabel bds:search ?__token__;
                bds:minRelevance "0.3";
                bds:matchAllTerms "false";
                bds:relevance ?score.
            }

                ?type (rdfs:subClassOf*) crm:E1_CRM_Entity ;
                rdfs:label ?typeLabel .
                FILTER(STRSTARTS(STR(?graph), "http://graph.lincsproject.ca/")) .
                FILTER(lang(?typeLabel)="en") .
                """
        autoQuery = autoQuery + "FILTER(?type in ({}))".format(', '.join(ranges))
        autoQuery = autoQuery +"""
                    BIND(CONCAT((CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(?valueLabel, " ("), str(?value)),") (a ", str(?type),") from ",str(?graph)))))) as ?label) .
                }
                ORDER BY DESC(?score)
                LIMIT 50
        """

    elif len(ranges) == 1:
        autoQuery = """SELECT ?value ?label WHERE { 
            GRAPH ?graph { 
                ?value rdf:type ?type  ;
                rdfs:label ?valueLabel.
            } .
            SERVICE bds:search {
                ?valueLabel bds:search ?__token__;
                bds:minRelevance "0.3";
                bds:matchAllTerms "false";
                bds:relevance ?score.
            }
                ?type (rdfs:subClassOf*) """
        autoQuery = autoQuery + ranges[0]
        autoQuery = autoQuery + """;
                rdfs:label ?typeLabel .
                FILTER(STRSTARTS(STR(?graph), "http://graph.lincsproject.ca/")) .
                FILTER(lang(?typeLabel)="en") .
                BIND(CONCAT((CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(?valueLabel, " ("), str(?value)),") (a ", str(?type),") from ",str(?graph)))))) as ?label) .
            }
                ORDER BY DESC(?score)
                LIMIT 50
        """

    else:
        autoQuery = """SELECT ?value ?label WHERE { 
            GRAPH ?graph { 
                ?value rdf:type ?type  ;
                rdfs:label ?valueLabel.
            } .
            SERVICE bds:search {
                ?valueLabel bds:search ?__token__;
                bds:minRelevance "0.3";
                bds:matchAllTerms "false";
                bds:relevance ?score.
            }
                ?type (rdfs:subClassOf*) crm:E1_CRM_Entity ;
                rdfs:label ?typeLabel .
                FILTER(STRSTARTS(STR(?graph), "http://graph.lincsproject.ca/")) .
                FILTER(lang(?typeLabel)="en") .
                BIND(CONCAT((CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(?valueLabel, " ("), str(?value)),") (a ", str(?type),") from ",str(?graph)))))) as ?label) .
            }
                ORDER BY DESC(?score)
                LIMIT 50
        """
    autoQuery = Literal(autoQuery)
    autosuggestion = BNode()

    g.add((
        URIRef('http://www.researchspace.org/resource/system/fieldDefinitionContainer'), 
        URIRef('http://www.w3.org/ns/ldp#contains'), 
        subject, 
        context
    ))
    g.add((
        subject, 
        URIRef('http://www.researchspace.org/resource/system/fields/autosuggestionPattern'), 
        autosuggestion, 
        context
    ))
    g.add((
        autosuggestion, 
        RDF['type'], 
        URIRef('http://spinrdf.org/sp#Query'), 
        context
    ))
    g.add((
        autosuggestion, 
        URIRef('http://spinrdf.org/sp#text'), 
        autoQuery, 
        context
    ))

    g.serialize(destination="new_assets/{}".format(fileName), format='trig')
    return g

def getDate():
    # we will need to update the timestamp
    return datetime.datetime.now()

def iterateFiles():
    for f in ldpFiles:
        try:
            g=Dataset()
            g.parse(f, format='trig')

        except Exception as e:
            print(e)


def getFiles():
    directory = '../lincs/ldp/assets'
    files = []
    # iterate over files in
    # that directory
    for filename in os.listdir(directory):
        f = os.path.join(directory, filename)
        # checking if it is a file
        files.append(f)
    return files

if __name__ == '__main__':
    main()