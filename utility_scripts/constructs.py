query1="""PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX prov: <http://www.w3.org/ns/prov#>
    PREFIX rso: <http://www.researchspace.org/ontology/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX lincs: <http://id.lincsproject.ca/>
    PREFIX : <http://www.researchspace.org/resource/>
    PREFIX void: <http://rdfs.org/ns/void#>
    PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
    PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
    PREFIX graph: <http://graph.lincsproject.ca/>
    
    CONSTRUCT {      
        ### GET THE GRAPH
        ?graph a crmdig:D1_Digital_Object ;
        a void:Dataset . 
        
        ### E55 TYPES AND CONCEPTS
        ?e55_type a crm:E55_Type ;
        rdfs:label ?e55_label .
        ?concept a skos:Concept ;
        rdfs:label ?concept_label .
        
        ### SUBJECT/TOPIC
        ?graph crm:P129_is_about ?keyword ;
        
        ### SUBGRAPHS
        crm:P106_is_composed_of ?subgraph ;
        
        ### STATUS
        crm:P2_has_type ?status .
        
        ### IMAGE
        ?image a ?image_type ;
        crm:P138_represents ?graph ;
        crm:P2_has_type ?image_type ;
        crm:P1_is_identified_by ?image_identifier .
        ?graph <http://www.researchspace.org/ontology/PX_has_main_representation> ?main_image .
        
        ### ANNOTATONS
        ?annotation crmdig:L43_annotates ?graph ;
                    a crmdig:D29_Annotation_Object ;
                    crm:P190_has_symbolic_content ?annotation_content ;
                    crm:P2_has_type ?annotation_type .
        
        ### DATA TRANSFER PROVENANCE
        ?dte crmdig:L14_was_transferred ?graph ;
                a crmdig:D12_Data_Transfer_Event ;
                crm:P4_has_time-span ?dte_timespan .
            ?dte_timepan crm:P82_at_some_time_within ?dte_date ;
                a crm:E52_Timespan .
    
        ### NOTES
        ?graph crm:P3_has_note ?note .
    
        ### SHORT TITLE
        ?graph crm:P1_is_identified_by ?short_title_iri . 
        ?short_title_iri a crm:E33_E41_Linguistic_Appelletion ;
            rdfs:label ?short_title ;
            crm:P2_has_type <http://vocab.getty.edu/aat/300417227> ;
            crm:P190_has_symbolic_content ?short_title .
    
        <http://vocab.getty.edu/aat/300417227> a crm:E55_Type, skos:Concept ;
        rdfs:label "alternative titles" .
    
        ### LONG TITLE
        ?graph crm:P1_is_identified_by ?long_title_iri .
        ?long_title_iri a crm:E33_E41_Linguistic_Appelletion ;
            rdfs:label ?long_title ;
            crm:P2_has_type <http://vocab.getty.edu/aat/300417209> ;
            crm:P190_has_symbolic_content ?long_title .
    
        <http://vocab.getty.edu/aat/300417209> a crm:E55_Type, skos:Concept ;
        rdfs:label "full titles" .
    
        ### SHORT DESCRIPTION
        ?short_desc_iri a crm:E33_Linguistic_Object ;
            crm:P129_is_about ?graph ;
            rdfs:label ?short_desc_label ;
            crm:P2_has_type <http://vocab.getty.edu/aat/300435416> ;
            crm:P190_has_symbolic_content ?short_desc .
    
        <http://vocab.getty.edu/aat/300435416> a crm:E55_Type, skos:Concept ;
            rdfs:label "descriptive notes" .
    
        ### LONG DESCRIPTION
        ?long_desc_iri a crm:E33_Linguistic_Object ;
            crm:P129_is_about ?graph ;
            rdfs:label ?long_desc_label ;
            crm:P2_has_type <http://vocab.getty.edu/aat/300411780> ;
            crm:P190_has_symbolic_content ?long_desc .

        <http://vocab.getty.edu/aat/300411780> a crm:E55_Type, skos:Concept ;
            rdfs:label "descriptions (documents)" .
    } WHERE { 
        graph <http://metadata.lincsproject.ca> {
            ?graph a crmdig:D1_Digital_Object ;
            a void:Dataset ;
            <http://purl.org/dc/terms/alternative> ?short_title ;
            <http://purl.org/dc/terms/title> ?long_title ;
            rdfs:comment ?short_desc ;
            <http://purl.org/dc/terms/description> ?long_desc .
            
            OPTIONAL {?e55_type a crm:E55_Type ;
                    rdfs:label ?e55_label .
                    }
            OPTIONAL {?concept a skos:Concept ;
                    rdfs:label ?concept_label .
                    }

            OPTIONAL { ?graph crm:P129_is_about ?keyword . }
            OPTIONAL { ?graph <http://www.cidoc-crm.org/cidoc-crm/P46_is_composed_of> ?subgraph. }
            OPTIONAL { ?graph lincs:status ?status }
                    


            BIND(IRI(CONCAT("http://temp.lincsproject.ca/shortTitle_", replace(?short_title," ","_"))) as ?short_title_iri)     
            BIND(IRI(CONCAT("http://temp.lincsproject.ca/longTitle_", replace(?short_title," ","_"))) as ?long_title_iri)  
            BIND(IRI(CONCAT("http://temp.lincsproject.ca/shortDesc_", replace(?short_title," ","_"))) as ?short_desc_iri)
            BIND(CONCAT("Short Description for ", ?short_title) as ?short_desc_label)
            BIND(IRI(CONCAT("http://temp.lincsproject.ca/longDesc_", replace(?short_title," ","_"))) as ?long_desc_iri)
            BIND(CONCAT("Description for ", ?short_title) as ?long_desc_label)
        }}"""
query2="""PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
    PREFIX owl: <http://www.w3.org/2002/07/owl#>
    PREFIX prov: <http://www.w3.org/ns/prov#>
    PREFIX rso: <http://www.researchspace.org/ontology/>
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
    PREFIX lincs: <http://id.lincsproject.ca/>
    PREFIX : <http://www.researchspace.org/resource/>
    PREFIX void: <http://rdfs.org/ns/void#>
    PREFIX crmdig: <http://www.ics.forth.gr/isl/CRMdig/>
    PREFIX crm: <http://www.cidoc-crm.org/cidoc-crm/>
    PREFIX graph: <http://graph.lincsproject.ca/>    
    CONSTRUCT {
      
        ### SOURCE
    
        ?source_iri crm:P130_shows_features_of ?graph ;
      		a crmdig:D1_Digital_Object ;
            rdfs:label ?source_label ;
            crm:P1_has_identifier ?source_identifier .
        ?source_identifier a crm:E42_Identifier ;
      		crm:P2_has_type lincs:sourceURL ;
            crm:P190_has_symbolic_content ?sourceURL ;
            rdfs:label ?sourceURL .
    
        ###CREATORS
        ?creation_iri a crmdig:D3_Formal_Derivation ;
            rdfs:label ?creation_label ;
            crmdig:L22_created_derivative ?graph ;
            crmdig:L21_used_as_derivation_source ?source_iri ;
            crmdig:L11_had_output ?graph ;
            crm:P14_carried_out_by ?creator .
        ?creator a ?creator_type ;
            owl:sameAs ?creator_sameAs ;
            rdfs:label ?creator_name .
        ?role_iri a crm:PC14_carried_out_by ;
        crm:P01_has_domain ?creation_iri ;
        crm:P02_has_range ?creator ;
        crm:P14.1_in_the_role_of ?creator_role .
    
        ### Converted LOD Source
        ?graph crm:P1_is_identified_by ?LODSourceidentifier .
        ?LODSourceidentifier a crm:E42_Identifier ;
            crm:P190_has_symbolic_content ?LODSourceContent ;
            crm:P2_has_type lincs:convertedLODSource .	
    
        ### LINCS Home URL
        ?graph crm:P1_is_identified_by ?homeIdentifier .
        ?homeIdentifier a crm:E42_Identifier ;
            crm:P190_has_symbolic_content ?homeContent ;
            crm:P2_has_type lincs:home .
    
        ### ARCHIVAL URL
    ?graph crm:P1_is_identified_by ?archivalIdentifier .
    ?archivalIdentifier a crm:E42_Identifier ;
            crm:P190_has_symbolic_content ?archivalContent ;
            crm:P2_has_type lincs:archivalURL .		
    
    ### Affiliated Website
    ?affiliatedWebsiteIdentifier a crm:E73_Information_Object ;
            rdfs:label ?affiliated_label ;
            crm:P129_is_about ?graph ;
            crm:P2_has_type lincs:affiliatedWebsite ;
            crm:P1_is_identified_by ?affiliatedWebsiteIRI .
    ?affiliatedWebsiteIRI a crm:E42_Identifier ;
            crm:P190_has_symbolic_content ?affiliatedWebsiteContent ;
            rdfs:label ?affiliatedWebsiteContent .
    
    }
    WHERE { graph <http://metadata.lincsproject.ca> {
        ?graph a void:Dataset ;
    	<http://purl.org/dc/terms/alternative> ?short_title .
        
        OPTIONAL { ?graph <http://purl.org/dc/terms/creator> ?creator .
                    ?creator rdfs:label ?creator_name ;
                        a ?creator_type .
                    OPTIONAL {?creator crm:P2_has_type ?creator_role .
                                ?creator_role rdfs:label ?creator_role_label .
                            }
                    OPTIONAL {?creator owl:sameAs ?creator_sameAs} 
                    }


        OPTIONAL { ?graph crm:P1_is_identified_by ?LODSourceIdentifier .
                    ?LODSourceIdentifier crm:P190_has_symbolic_content ?LODSourceContent ;
                        crm:P2_has_type lincs:ConvertedLODSource .		
                    }
        OPTIONAL { ?graph crm:P1_is_identified_by ?homeIdentifier .
                    ?homeIdentifier crm:P190_has_symbolic_content ?homeContent ;
                        crm:P2_has_type lincs:home .		
                    }
                
        OPTIONAL {  	?graph crm:P1_is_identified_by ?archivalIdentifier .
                        ?archivalIdentifier crm:P190_has_symbolic_content ?archivalContent ;
                            crm:P2_has_type lincs:archivalURL .		
                    }
        
        OPTIONAL {	?graph crm:P1_is_identified_by ?affiliatedWebsiteIdentifier .
                        ?affiliatedWebsiteIdentifier crm:P190_has_symbolic_content ?affiliatedWebsiteContent ;
                            crm:P2_has_type lincs:affiliatedWebsite .	
                    }
        OPTIONAL { ?graph crm:P138i_has_representation ?image .
                        ?image a ?image_type ;
                        crm:p2_has_type ?image_type ;
                        crm:P1_is_identified_by ?image_identifier .
                    }
        OPTIONAL { ?graph <http://www.researchspace.org/ontology/PX_has_main_representation> ?main_image }
        OPTIONAL { ?graph prov:wasDerivedFrom ?sourceURL }
        OPTIONAL { ?annotation crmdig:L43_annotates ?graph ;
                        crm:P190_has_symbolic_content ?annotation_content ;
                        crm:P2_has_type ?annotation_type .
                    }
        OPTIONAL { ?dte crmdig:L14_was_transferred ?graph ;
                    crm:P4_has_time-span ?dte_timespan .
                    ?dte_timepan crm:P82_at_some_time_within ?dte_date .
                    }
        OPTIONAL { ?graph <http://purl.org/dc/terms/language> ?language }
        OPTIONAL { ?graph crm:P3_has_note ?note}

        BIND(IRI(CONCAT("http://temp.lincsproject.ca/creation_event_", replace(?short_title," ","_"))) as ?creation_iri)     
        BIND(CONCAT("Creation of ", ?short_title) as ?creation_label)
        BIND(IRI(CONCAT("http://temp.lincsproject.ca/source_for_", replace(?short_title," ","_"))) as ?source_iri)
        BIND(CONCAT("Source for ", ?short_title) as ?source_label)
        BIND(IRI(CONCAT("http://temp.lincsproject.ca/source_identifier_for_", replace(?short_title," ","_"))) as ?source_identifier)
        BIND(IRI(CONCAT("http://temp.lincsproject.ca/role_for_", replace(?creator_name," ","_"))) as ?role_iri)
        BIND(IRI(CONCAT("http://temp.lincsproject.ca/affiliated_website_for_", replace(?short_title," ","_"))) as ?affiliatedWebsiteIRI)
        BIND(CONCAT("Affiliated Website for ", ?short_title) as ?affiliated_label)
  }}"""