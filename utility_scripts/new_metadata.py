from rdflib.namespace import RDF, RDFS, OWL, SKOS
from rdflib import URIRef, Literal, XSD, Graph, Namespace
import json
from datetime import datetime
import requests


def main():
    data = json.load(open("metadata_template.json", "r"))
    g = Graph()

    CRMDig = Namespace("http://www.ics.forth.gr/isl/CRMdig/")
    CRM = Namespace("http://www.cidoc-crm.org/cidoc-crm/")
    VOID = Namespace("http://rdfs.org/ns/void#")
    LINCS = Namespace("http://id.lincsproject.ca/")
    g.bind("crmdig", CRMDig)
    g.bind("lincs", LINCS)
    g.bind("crm", CRM)

    #create the graph ID
    g.add( ( URIRef(data['dataset_id']), RDF['type'], CRMDig["D1_Digital_Object"] ) )
    g.add( ( URIRef(data['dataset_id']), RDF['type'], VOID["Dataset"] ) )
    g.add( ( URIRef(data['dataset_id']), CRM['P2_has_type'], LINCS["draft"] ) )

    #full title
    UID = getUID(1)
    g.add( ( LINCS[UID[0]], RDF["type"], CRM["E33_E41_Linguistic_Appellation"] ) )
    g.add( ( LINCS[UID[0]], CRM["P2_has_type"], URIRef("http://vocab.getty.edu/aat/300417209") ) )
    g.add( ( LINCS[UID[0]], CRM["P190_has_symbolic_content"], Literal(data['long_title']) ) )
    g.add( ( URIRef(data['dataset_id']), CRM["P1_is_identified_by"], LINCS[UID[0]] ) )

    #short title
    UID = getUID(1)
    g.add( ( LINCS[UID[0]], RDF["type"], CRM["E33_E41_Linguistic_Appellation"] ) )
    g.add( ( LINCS[UID[0]], CRM["P2_has_type"], URIRef("http://vocab.getty.edu/aat/300417227") ) )
    g.add( ( LINCS[UID[0]], CRM["P190_has_symbolic_content"], Literal(data['short_title']) ) )
    g.add( ( URIRef(data['dataset_id']), CRM["P1_is_identified_by"], LINCS[UID[0]] ) )

    #long description
    UID = getUID(1)
    g.add( ( LINCS[UID[0]], RDF["type"], CRM["E33_Linguistic_Object"] ) )
    g.add( ( LINCS[UID[0]], CRM["P2_has_type"], URIRef("http://vocab.getty.edu/aat/300411780") ) )
    g.add( ( LINCS[UID[0]], CRM["P190_has_symbolic_content"], Literal(data['long_description']) ) )
    g.add( ( LINCS[UID[0]], CRM["P129_is_about"], URIRef(data['dataset_id']) ) )

    #long description
    UID = getUID(1)
    g.add( ( LINCS[UID[0]], RDF["type"], CRM["E33_Linguistic_Object"] ) )
    g.add( ( LINCS[UID[0]], CRM["P2_has_type"], URIRef("http://vocab.getty.edu/aat/300435416") ) )
    g.add( ( LINCS[UID[0]], CRM["P190_has_symbolic_content"], Literal(data['short_description']) ) )
    g.add( ( LINCS[UID[0]], CRM["P129_is_about"], URIRef(data['dataset_id']) ) )

    #application profile
    if data["application_profile"]:
        UID = getUID(2)
        
        g.add( ( LINCS[UID[0]], RDF["type"], CRM["E73_Information_Object"] ) )
        g.add( ( LINCS[UID[0]], CRM["P129_is_about"], URIRef(data['dataset_id']) ) )
        g.add( ( LINCS[UID[0]], RDFS["label"], Literal("Application Profile for " + data["short_title"]) ) )
        g.add( ( LINCS[UID[0]], CRM["P2_has_type"], LINCS["applicationProfile"] ) )
        g.add( ( LINCS[UID[0]], CRM["P1_is_identified_by"], LINCS[UID[1]] ) )

        g.add( ( LINCS[UID[1]], RDF["type"], CRM["E42_Identifier"] ) )
        g.add( ( LINCS[UID[1]], CRM["P190_has_symbolic_content"], Literal(data["application_profile"]) ) )

    #project search page
    UID = getUID(1)
    g.add( ( LINCS[UID[0]], RDF["type"], CRM["E42_Identifier"] ) )
    g.add( ( LINCS[UID[0]], CRM["P2_has_type"], LINCS["home"] ) )
    g.add( ( LINCS[UID[0]], CRM["P190_has_symbolic_content"], Literal(data['search_page']) ) )
    g.add( ( URIRef(data['dataset_id']), CRM["P1_is_identified_by"], LINCS[UID[0]] ) )

    #archival url
    if data["archival_url"]:
        UID = getUID(1)
        g.add( ( LINCS[UID[0]], RDF["type"], CRM["E42_Identifier"] ) )
        g.add( ( LINCS[UID[0]], CRM["P2_has_type"], LINCS["archivalURL"] ) )
        g.add( ( LINCS[UID[0]], CRM["P190_has_symbolic_content"], Literal(data['archival_url']) ) )
        g.add( ( URIRef(data['dataset_id']), CRM["P1_is_identified_by"], LINCS[UID[0]] ) )

    #converted data
    if data['converted_data']:
        UID = getUID(1)
        g.add( ( LINCS[UID[0]], RDF["type"], CRM["E42_Identifier"] ) )
        g.add( ( LINCS[UID[0]], CRM["P2_has_type"], LINCS["ConvertedLODSource"] ) )
        g.add( ( LINCS[UID[0]], CRM["P190_has_symbolic_content"], Literal(data['converted_data']) ) )
        g.add( ( URIRef(data['dataset_id']), CRM["P1_is_identified_by"], LINCS[UID[0]] ) )

    # affiliated websites (a list of strings)
    if data["affiliated_websites"]:
        for content in data["affiliated_websites"]:
            UID = getUID(2)
            
            g.add( ( LINCS[UID[0]], RDF["type"], CRM["E73_Information_Object"] ) )
            g.add( ( LINCS[UID[0]], CRM["P129_is_about"], URIRef(data['dataset_id']) ) )
            g.add( ( LINCS[UID[0]], RDFS["label"], Literal("Affiliated Website for " + data["short_title"]) ) )
            g.add( ( LINCS[UID[0]], CRM["P2_has_type"], LINCS["affiliatedWebsite"] ) )
            g.add( ( LINCS[UID[0]], CRM["P1_is_identified_by"], LINCS[UID[1]] ) )
            
            g.add( ( LINCS[UID[1]], RDF["type"], CRM["E42_Identifier"] ) )
            g.add( ( LINCS[UID[1]], CRM["P190_has_symbolic_content"], Literal(content) ) )
            g.add( ( LINCS[UID[1]], RDFS["label"], Literal(content) ) )


    # entity disclaimer
    if data['entity_disclaimer']:
        UID = getUID(1)
        g.add( ( LINCS[UID[0]], RDF["type"], CRMDig["D29_Annotation_Object"] ) )
        g.add( ( LINCS[UID[0]], CRM["P2_has_type"], LINCS["entityAnnotation"] ) )
        g.add( ( LINCS[UID[0]], CRM["P190_has_symbolic_content"], Literal(data['entity_disclaimer']) ) )
        g.add( ( LINCS[UID[0]], CRMDig["L43_annotates"], URIRef(data['dataset_id']) ) )

    # dataset disclaimer
    if data['dataset_disclaimer']:
        UID = getUID(1)
        g.add( ( LINCS[UID[0]], RDF["type"], CRMDig["D29_Annotation_Object"] ) )
        g.add( ( LINCS[UID[0]], CRM["P2_has_type"], LINCS["dataSetAnnotation"] ) )
        g.add( ( LINCS[UID[0]], CRM["P190_has_symbolic_content"], Literal(data['dataset_disclaimer']) ) )
        g.add( ( LINCS[UID[0]], CRMDig["L43_annotates"], URIRef(data['dataset_id']) ) )

    # note
    if data['note']:
        UID = getUID(1)
        g.add( ( URIRef(data['dataset_id']), CRM["P3_has_note"], Literal(data['note']) ))

    # rights
    if data['rights']:
        g.add( ( URIRef(data['dataset_id']), CRM["P104_is_subject_to"], URIRef(data['rights']) ) )

    # is composed of (subgraphs)
    if data["is_composed_of"]:
        for subGraph in data["is_composed_of"]:
            g.add( ( URIRef(data['dataset_id']), CRM["P106_is_composed_of"], URIRef(subGraph) ) )

    # subject/topic/keyword
    if data["subject_topic"]:
        for topic in data["subject_topic"]:
            g.add( ( URIRef(topic), CRM["P129_is_about"], URIRef(data['dataset_id'])  ) )

    #create the dataset derivation object
    UID = getUID(2)
    source_object = LINCS[UID[0]]
    derivative_object = LINCS[UID[1]]
    g.add( ( source_object, RDF['type'], CRMDig["D1_Digital_Object"] ) )
    g.add( ( source_object, CRM["P130_shows_features_of"], URIRef(data['dataset_id']) ) )

    g.add( ( derivative_object, RDF['type'], CRMDig["D3_Formal_Derivation"] ) )
    g.add( ( derivative_object, CRMDig['L21_used_as_derivation_source'], source_object ) )
    g.add( ( derivative_object, CRMDig['L22_created_derivative'], URIRef(data['dataset_id']) ) )

    #source url
    if data["source_url"]:
        UID = getUID(1)
        g.add( ( LINCS[UID[0]], RDF["type"], CRM["E42_Identifier"] ) )
        g.add( ( LINCS[UID[0]], CRM["P2_has_type"], LINCS["sourceURL"] ) )
        g.add( ( LINCS[UID[0]], CRM["P190_has_symbolic_content"], Literal(data["source_url"]) ) )
        g.add( ( source_object, CRM["P1_is_identified_by"], LINCS[UID[0]] ) )

    #contributors

    for contributor in data["carried_out_by"]:
        if contributor["existing_id"]:
            pass
        else:
            UID = getUID(2)
            role = LINCS[UID[0]]
            actor = LINCS[UID[1]]
            g.add( (role, RDF["type"], CRM["PC14_carried_out_by"] ) )
            g.add( (role, CRM["P02_has_range"], actor ) )
            g.add( (role, CRM["P01_has_domain"], derivative_object ) )

            g.add( ( actor, RDF["type"], CRM[contributor["entity_type"]] ) )
            g.add( ( actor, RDFS["label"], Literal(contributor["name"]) ) )
            g.add( ( derivative_object, CRM["P14_carried_out_by"], actor ))

            if contributor["same_as"]:
                g.add( ( actor, OWL["sameAs"], URIRef(contributor["same_as"]) ) )           
            if contributor["existing_role_type"]:
                g.add( ( role, CRM["P14.1_in_the_role_of"], URIRef(contributor["existing_role_type"]) ) )
            if contributor["new_role_type"]:
                g.add( (URIRef(contributor["new_role_type"]), RDF["type"], CRM['E55_Type'] ) )
                g.add( (URIRef(contributor["new_role_type"]), RDF["type"], SKOS['Concept'] ) )
                g.add( ( role, CRM["P14.1_in_the_role_of"], URIRef(contributor["new_role_type"]) ) )


    print(g.serialize())
    #datetime (not implementing yet)
    # data["period_start"]
    # data["period_end"]


def getUID(n):
    return requests.get('https://uid.lincsproject.ca/generatemultipleUID?num=' + str(n)).json()['uids']

if __name__ == '__main__':
    main()

