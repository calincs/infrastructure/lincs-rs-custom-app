
from SPARQLWrapper import SPARQLWrapper, POST, DIGEST, GET, BASIC
from load_config import ENDPOINT, DATA



DATA = ["LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_manual_uncertainty.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_0.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_1.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_2.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_3.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_4.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_5.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_6.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_7.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_8.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_9.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_10.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_11.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_12.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_13.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_14.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_15.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_16.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_17.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_18.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_19.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_20.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_21.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_22.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_23.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_24.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_25.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_26.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_27.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_28.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_29.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_30.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_31.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_32.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_33.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_34.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_35.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_36.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_37.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_38.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_39.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_40.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_41.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_42.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_43.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/dcb_webannotations_44.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_0.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_1.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_2.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_3.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_4.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_5.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_6.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_7.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_8.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_9.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_10.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_11.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_12.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_13.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_14.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_15.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_16.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_17.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_18.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_19.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>",
"LOAD <https://aux.lincsproject.ca/public/converted_data/datasets/hist-cdns/hist-cdns_references.ttl> INTO GRAPH <http://graph.lincsproject.ca/hist-canada/hist-cdns>"]


ENDPOINT = "https://bg-review.lincsproject.ca/sparql"
sparql = SPARQLWrapper(ENDPOINT)
#sparql.setHTTPAuth(DIGEST)
sparql.setCredentials(user="lincs", passwd="")
sparql.setMethod(POST)
for line in DATA:
    sparql.setQuery(line)
    results = sparql.query()
    print(results.response.read())


   