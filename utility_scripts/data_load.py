from SPARQLWrapper import SPARQLWrapper, POST, DIGEST, GET, BASIC
from load_config import ENDPOINT, DATA

sparql = SPARQLWrapper(ENDPOINT)
sparql.setHTTPAuth(DIGEST)
sparql.setCredentials("admin", "admin")
sparql.setMethod(POST)
for line in DATA:
    sparql.setQuery(line)
    results = sparql.query()
    print(results.response.read())