</h1>
    <semantic-form
                id='authoring-form'
                persistence='{"type": "sparql", "targetGraphIRI": "{{graphIRI}}"}'
                subject = '{{node}}'
                post-action='redirect'
                fields='[[fieldDefinitions
                                    classtype="http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
                                    P2_has_type_auto="http://www.cidoc-crm.org/cidoc-crm/P2_has_type"
                                    P2_has_type_tree="http://www.cidoc-crm.org/cidoc-crm/P2_has_type"
                                    same_as_auto="http://www.w3.org/2002/07/owl#sameAs"
                                    same_as_man="http://www.w3.org/2002/07/owl#sameAs"
                                    P129_is_about="http://www.cidoc-crm.org/cidoc-crm/P129_is_about"
                                    label="http://www.w3.org/2000/01/rdf-schema#label"
                                    