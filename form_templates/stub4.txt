"></semantic-form-hidden-input>
        <semantic-form-text-input for="label"></semantic-form-text-input>
        <semantic-form-autocomplete-input for="P2_has_type_auto" label="has type (full-text lookup)"></semantic-form-autocomplete-input>
        <semantic-form-tree-picker-input for="P2_has_type_tree" label="has type (vocabulary tree)"></semantic-form-tree-picker-input>
        <semantic-form-autocomplete-input for="same_as_auto" label="same as (lookup)"></semantic-form-autocomplete-input>
        <semantic-form-text-input for="same_as_man" label="same as (manual input)"></semantic-form-text-input>
        <semantic-form-autocomplete-input for="P129_is_about"></semantic-form-autocomplete-input>
        