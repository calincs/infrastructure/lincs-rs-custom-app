#!/bin/sh

set -e

# if $BASIC_AUTH_ADMIN exists, override the password
if [ -n "$BASIC_AUTH_ADMIN" ] ; then
  sed -i "s%admin=password,admin%admin=${BASIC_AUTH_ADMIN},admin%" "/apps/lincs/config/shiro.ini"
fi

if [ "$GITLAB_ENVIRONMENT_NAME" = "production" ] ; then
  rm /apps/lincs/assets/css_variables.css
  mv /apps/lincs/assets/css_variables_production.css /apps/lincs/assets/css_variables.css
  rm /apps/lincs/config/ui.prop
  mv /apps/lincs/config/ui_prod.prop /apps/lincs/config/ui.prop
fi

if [ "$GITLAB_ENVIRONMENT_NAME" = "rs-review" ] ; then
  rm /apps/lincs/config/ui.prop
  mv /apps/lincs/config/ui_review.prop /apps/lincs/config/ui.prop
fi



# kick off the upstream RS entrypoint
exec /entrypoint.sh "$@"
