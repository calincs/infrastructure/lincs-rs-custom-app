@prefix ns1: <https://rs.stage.lincsproject.ca/container/fieldDefinitionContainer/has_parent/> .
@prefix ns2: <http://www.researchspace.org/resource/system/fields/> .
@prefix ns3: <http://spinrdf.org/sp#> .
@prefix ns4: <http://www.w3.org/ns/ldp#> .
@prefix ns5: <file:///home/zschoenb/LINCS/lincs-dev/lincs-rs-custom-app/lincs/ldp/assets/> .
@prefix ns6: <urn:x-rdflib:> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

ns1:context {
    <http://www.researchspace.org/resource/system/fieldDefinitionContainer> ns4:contains <https://rs.stage.lincsproject.ca/container/fieldDefinitionContainer/has_parent> .

    <https://rs.stage.lincsproject.ca/container/fieldDefinitionContainer/has_parent> a ns2:Field,
            ns4:Resource,
            prov:Entity ;
        rdfs:label "has parent" ;
        ns2:autosuggestionPattern [ a ns3:Query ;
                ns3:text """SELECT ?value ?label WHERE { 
            GRAPH ?graph { 
                ?value rdf:type ?type  ;
                rdfs:label ?valueLabel.
            } .
            SERVICE bds:search {
                ?valueLabel bds:search ?__token__;
                bds:minRelevance "0.3";
                bds:matchAllTerms "false";
                bds:relevance ?score.
            }
                ?type (rdfs:subClassOf*) <http://www.cidoc-crm.org/cidoc-crm/E21_Person>;
                rdfs:label ?typeLabel .
                FILTER(STRSTARTS(STR(?graph), "http://graph.lincsproject.ca/")) .
                FILTER(lang(?typeLabel)="en") .
                BIND(CONCAT((CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(?valueLabel, " ("), str(?value)),") (a ", str(?type),") from ",str(?graph)))))) as ?label) .
            }
                ORDER BY DESC(?score)
                LIMIT 50
        """ ] ;
        ns2:deletePattern [ a ns3:Query ;
                ns3:text "DELETE { $subject <http://www.cidoc-crm.org/cidoc-crm/P152_has_parent> $value . } WHERE { $subject <http://www.cidoc-crm.org/cidoc-crm/P152_has_parent> $value . }" ] ;
        ns2:domain <http://www.cidoc-crm.org/cidoc-crm/E39_Actor> ;
        ns2:insertPattern [ a ns3:Query ;
                ns3:text "INSERT { $subject <http://www.cidoc-crm.org/cidoc-crm/P152_has_parent> $value . } WHERE {}" ] ;
        ns2:maxOccurs "unbound" ;
        ns2:minOccurs "0" ;
        ns2:range <http://www.cidoc-crm.org/cidoc-crm/E21_Person> ;
        ns2:selectPattern [ a ns3:Query ;
                ns3:text """SELECT ?value WHERE {
  $subject <http://www.cidoc-crm.org/cidoc-crm/P152_has_parent> ?value . 
}""" ] ;
        ns2:xsdDatatype xsd:anyURI ;
        rdfs:comment """This property associates an instance of E21 Person with another instance of E21 Person who plays the role of the first instance’s parent, regardless of whether the relationship is biological parenthood, assumed or pretended biological parenthood or an equivalent legal status of rights and obligations obtained by a social or legal act.
	This property is, among others, a shortcut of the fully developed paths from ‘E21Person’ through ‘P98i was born’, ‘E67 Birth’, ‘P96 by mother’ to ‘E21 Person’, and from ‘E21Person’ through ‘P98i was born’, ‘E67 Birth’, ‘P97 from father’ to ‘E21 Person’.
	""" ;
        prov:generatedAtTime "2021-12-02T16:35:18.749000+00:00"^^xsd:dateTime ;
        prov:wasAttributedTo <http://www.researchspace.org/resource/user/zschoenb> .
}

