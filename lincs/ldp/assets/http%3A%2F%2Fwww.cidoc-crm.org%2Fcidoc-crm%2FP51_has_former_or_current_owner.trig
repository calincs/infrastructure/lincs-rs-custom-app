@prefix ns1: <file:///home/zschoenb/LINCS/lincs-dev/lincs-rs-custom-app/lincs/ldp/assets/> .
@prefix ns2: <http://www.cidoc-crm.org/cidoc-crm/P51_has_former_or_current_owner/> .
@prefix ns3: <http://www.w3.org/ns/ldp#> .
@prefix ns4: <http://www.researchspace.org/resource/system/fields/> .
@prefix ns5: <http://spinrdf.org/sp#> .
@prefix ns6: <urn:x-rdflib:> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

ns2:context {
    <http://www.researchspace.org/resource/system/fieldDefinitionContainer> ns3:contains <http://www.cidoc-crm.org/cidoc-crm/P51_has_former_or_current_owner> .

    <http://www.cidoc-crm.org/cidoc-crm/P51_has_former_or_current_owner> a ns4:Field,
            ns3:Resource,
            prov:Entity ;
        rdfs:label "hat früheren oder derzeitigen Besitzer "@de,
            "έχει ή είχε ιδιοκτήτη"@el,
            "has former or current owner"@en,
            "est ou a été possédée par"@fr,
            "é ou foi propriedade de"@pt,
            "имеет бывшего или текущего владельца"@ru,
            "有前任或现任物主"@zh ;
        ns4:autosuggestionPattern [ a ns5:Query ;
                ns5:text """SELECT ?value ?label WHERE { 
            GRAPH ?graph { 
                ?value rdf:type ?type  ;
                rdfs:label ?valueLabel.
            } .
            SERVICE bds:search {
                ?valueLabel bds:search ?__token__;
                bds:minRelevance "0.3";
                bds:matchAllTerms "false";
                bds:relevance ?score.
            }
                ?type (rdfs:subClassOf*) <http://www.cidoc-crm.org/cidoc-crm/E39_Actor>;
                rdfs:label ?typeLabel .
                FILTER(STRSTARTS(STR(?graph), "http://graph.lincsproject.ca/")) .
                FILTER(lang(?typeLabel)="en") .
                BIND(CONCAT((CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(?valueLabel, " ("), str(?value)),") (a ", str(?type),") from ",str(?graph)))))) as ?label) .
            }
                ORDER BY DESC(?score)
                LIMIT 50
        """ ] ;
        ns4:category <http://www.researchspace.org/resource/system/formContainer/category/artworks>,
            <http://www.researchspace.org/resource/system/formContainer/category/default> ;
        ns4:deletePattern [ a ns5:Query ;
                ns5:text "DELETE { $subject <http://www.cidoc-crm.org/cidoc-crm/P51_has_former_or_current_owner> $value . } WHERE { $subject <http://www.cidoc-crm.org/cidoc-crm/P51_has_former_or_current_owner> $value . }" ] ;
        ns4:domain <http://www.cidoc-crm.org/cidoc-crm/E18_Physical_Thing> ;
        ns4:insertPattern [ a ns5:Query ;
                ns5:text "INSERT { $subject <http://www.cidoc-crm.org/cidoc-crm/P51_has_former_or_current_owner> $value . } WHERE {}" ] ;
        ns4:maxOccurs "unbound" ;
        ns4:minOccurs "0" ;
        ns4:range <http://www.cidoc-crm.org/cidoc-crm/E39_Actor> ;
        ns4:selectPattern [ a ns5:Query ;
                ns5:text """SELECT ?value WHERE {
  $subject <http://www.cidoc-crm.org/cidoc-crm/P51_has_former_or_current_owner> ?value . 
}""" ] ;
        ns4:xsdDatatype xsd:anyURI ;
        rdfs:comment """This property identifies the E39 Actor that is or has been the legal owner (i.e. title holder) of an instance of E18 Physical Thing at some time.
The distinction with P52 has current owner (is current owner of) is that P51 has former or current owner (is former or current owner of) does not indicate whether the specified owners are current. P51 has former or current owner (is former or current owner of) is a shortcut for the more detailed path from E18 Physical Thing through P24 transferred title of (changed ownership through), E8 Acquisition, P23 transferred title from (surrendered title through), or P22 transferred title to (acquired title through) to E39 Actor.
""" ;
        prov:generatedAtTime "2021-12-02T16:34:01.294000+00:00"^^xsd:dateTime ;
        prov:wasAttributedTo <http://www.researchspace.org/resource/user/zschoenb> .
}

