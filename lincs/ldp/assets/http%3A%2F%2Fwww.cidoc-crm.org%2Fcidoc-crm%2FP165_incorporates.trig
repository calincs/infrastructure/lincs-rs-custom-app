@prefix ns1: <http://www.cidoc-crm.org/cidoc-crm/P165_incorporates/> .
@prefix ns2: <http://www.w3.org/ns/ldp#> .
@prefix ns3: <http://spinrdf.org/sp#> .
@prefix ns4: <http://www.researchspace.org/resource/system/fields/> .
@prefix ns5: <file:///home/zschoenb/LINCS/lincs-dev/lincs-rs-custom-app/lincs/ldp/assets/> .
@prefix ns6: <urn:x-rdflib:> .
@prefix prov: <http://www.w3.org/ns/prov#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

ns1:context {
    <http://www.researchspace.org/resource/system/fieldDefinitionContainer> ns2:contains <http://www.cidoc-crm.org/cidoc-crm/P165_incorporates> .

    <http://www.cidoc-crm.org/cidoc-crm/P165_incorporates> a ns4:Field,
            ns2:Resource,
            prov:Entity ;
        rdfs:label "incorporates"@en ;
        ns4:autosuggestionPattern [ a ns3:Query ;
                ns3:text """SELECT ?value ?label WHERE { 
            GRAPH ?graph { 
                ?value rdf:type ?type  ;
                rdfs:label ?valueLabel.
            } .
            SERVICE bds:search {
                ?valueLabel bds:search ?__token__;
                bds:minRelevance "0.3";
                bds:matchAllTerms "false";
                bds:relevance ?score.
            }
                ?type (rdfs:subClassOf*) <http://www.cidoc-crm.org/cidoc-crm/E90_Symbolic_Object>;
                rdfs:label ?typeLabel .
                FILTER(STRSTARTS(STR(?graph), "http://graph.lincsproject.ca/")) .
                FILTER(lang(?typeLabel)="en") .
                BIND(CONCAT((CONCAT(CONCAT(CONCAT(CONCAT(CONCAT(?valueLabel, " ("), str(?value)),") (a ", str(?type),") from ",str(?graph)))))) as ?label) .
            }
                ORDER BY DESC(?score)
                LIMIT 50
        """ ] ;
        ns4:category <http://www.researchspace.org/resource/system/formContainer/category/default> ;
        ns4:deletePattern [ a ns3:Query ;
                ns3:text "DELETE { $subject <http://www.cidoc-crm.org/cidoc-crm/P165_incorporates> $value . } WHERE { $subject <http://www.cidoc-crm.org/cidoc-crm/P165_incorporates> $value . }" ] ;
        ns4:domain <http://www.cidoc-crm.org/cidoc-crm/E73_Information_Object> ;
        ns4:insertPattern [ a ns3:Query ;
                ns3:text "INSERT { $subject <http://www.cidoc-crm.org/cidoc-crm/P165_incorporates> $value . } WHERE {}" ] ;
        ns4:maxOccurs "unbound" ;
        ns4:minOccurs "0" ;
        ns4:range <http://www.cidoc-crm.org/cidoc-crm/E90_Symbolic_Object> ;
        ns4:selectPattern [ a ns3:Query ;
                ns3:text """SELECT ?value WHERE {
  $subject <http://www.cidoc-crm.org/cidoc-crm/P165_incorporates> ?value . 
}""" ] ;
        ns4:xsdDatatype xsd:anyURI ;
        rdfs:comment """This property associates an instance of E73 Information Object with an instance of E90 Symbolic Object (or any of its subclasses) that was included in it.
This property makes it possible to recognise the autonomous status of the incorporated signs, which were created in a distinct context, and can be incorporated in many distinct self-contained expressions, and to highlight the difference between structural and accidental whole-part relationships between conceptual entities.
It accounts for many cultural facts that are quite frequent and significant: the inclusion of a poem in an anthology, the re-use of an operatic aria in a new opera, the use of a reproduction of a painting for a book cover or a CD booklet, the integration of textual quotations, the presence of lyrics in a song that sets those lyrics to music, the presence of the text of a play in a movie based on that play, etc.
In particular, this property allows for modelling relationships of different levels of symbolic specificity, such as the natural language words making up a particular text, the characters making up the words and punctuation, the choice of fonts and page layout for the characters.
A digital photograph of a manuscript page incorporates the text of the manuscript page.
	""" ;
        prov:generatedAtTime "2021-12-02T16:35:01.929000+00:00"^^xsd:dateTime ;
        prov:wasAttributedTo <http://www.researchspace.org/resource/user/zschoenb> .
}

