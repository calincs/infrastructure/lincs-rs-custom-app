class lincsTimelineVert extends HTMLElement {
    constructor() {
        super();
        this.innerHTML = "";
        var template = 
        `
        <style id="styleBlock">.vtimeline::before {height: 150%;}</style>
        <div style="margin: auto; display: flex; flex-direction: row; justify-content: center;">
            <div class="vcontainer">
                <div class="vtimeline">
                    <ul id="vlist">
                        
                    </ul>
                </div>
            </div>
            <div style="width: 40px; position: relative; margin-left: 8px;">
                <span style="position: absolute; top: 0px;" id="yearDisplay"></span>
            </div>
        </div>
        `;
        this.innerHTML = template;
        this.styleBlock = this.querySelector("#styleBlock");
        this.container = this.querySelector(".vcontainer");
        this.list = this.querySelector("#vlist");
        this.yearDisplay = this.querySelector("#yearDisplay");
        this.years = [];
    }
    connectedCallback() {
        var _this = this;
        var types = [];
        var colours = ["background-color: rgb(255, 138, 138);", "background-color: rgb(112, 215, 255);", "background-color: rgb(205, 135, 255);", "background-color: rgb(102, 217, 138);", "background-color: rgb(69, 131, 255);", "background-color: rgb(255, 174, 69);", "background-color: rgb(245, 223, 83);", "background-color: rgb(255, 110, 194);"];
        var info = document.getElementsByClassName("qinfo");
        var html = "";

        for (var i = 0; i < info.length; i++) {
            var entry = info[i].textContent.split(";");
            entry[1] = entry[1].split("T")[0];

            this.years.push(entry[1].split("-")[0]);

            var ind = types.indexOf(entry[2]);
            if (ind == -1) {
                types.push(entry[2]);
                ind = types.length - 1;
            }

            if (colours[ind] == null) {
                entry[2] = "gray";
            } else {
                entry[2] = colours[ind];
            }

            html += `<li>
                         <div class="content" style="position: relative">
                             <h3 style="` + entry[2] + `">` + entry[0] + `</h3>
                             <div class="vdate-hover" style="background-color: rgba(105,105,105, 0.7); position: absolute; width: 100%; height: 100%; top: 0px; left: 0px; display: flex; align-items: center; justify-content: center;">
                                 <a href="/resource/?uri=` + entry[3] + `">
                                     <i style="font-size: 25px; color: white;" padding: 0px 8px;" class="rs-icon rs-icon-page"></i>
                                 </a>
                                 <a href="/resource/:ThinkingFrames?view=knowledge-map&resource=` + entry[3] + `">
                                     <i style="color: white; font-size: 25px; padding: 0px 8px;" class="rs-icon rs-icon-diagram"></i>
                                 </a>
                             </div>
                         </div>
                         <div class="vpoint" style="` + entry[2] + `"></div>
                         <div class="date">
                             <h4 style="font-size: 16px; ` + entry[2] + `">` + entry[1] + `</h4>
                         </div>
                     </li>`
        }

        this.list.innerHTML = html;
        this.resetTimlineHeight();

        window.addEventListener("resize", function() {
            _this.resetTimlineHeight();
        });

        this.container.addEventListener("scroll", function() {
            _this.updateScrollInfo()
        }, false);

        this.updateScrollInfo();
    }
    resetTimlineHeight() {
        this.styleBlock.innerHTML = ".vtimeline::before {height: 150%;}";
        this.styleBlock.innerHTML = ".vtimeline::before {height: " + (this.container.scrollHeight + 5) + "px;}";
    }
    updateScrollInfo() {
        var curVal = this.container.scrollTop / this.list.offsetHeight;
        var date = Math.floor(this.years.length * curVal);
        if (date != (this.years.length - 1) && date != 0) {
            date += 1;
        }

        this.yearDisplay.textContent = this.years[date];
        this.yearDisplay.style.top = ((this.container.offsetHeight - 20) * curVal) + "px";
    }
}

customElements.define('lincs-timeline-vertical', lincsTimelineVert);