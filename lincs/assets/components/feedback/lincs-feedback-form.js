class lincsFeedbackForm extends HTMLElement {
    constructor() {
        super();
            this.innerHTML = `
            <div id="lincsFeedbackFormContainer">
                <h1>Feedback Form</h1>
                <form id="lincsFeedbackForm" enctype="multipart/form-data" action="/rest/feedback" method="post">
                    <label for="name">Name: </label><br>
                    <input id="name" type="text" name="name"/><br>
                    <label for="email">Email Address: </label><br>
                    <input id="email" type="text" name="email"/><br>
                    <label for="title">Feedback Title/Summary: </label><br>
                    <input id="title" type="text" name="title"/><br>
                    <label for="description">Feedback Description: </label><br>
                    <textarea id="description" type="text" name="description"></textarea><br>
                    <label for="type">Type of Feedback: </label><br>
                    <label for="ui">UX&nbsp;</label><input id="ux" type="radio" value="ux" name="type" checked/><br>
                    <label for="data">Data&nbsp;</label><input id="data" type="radio" value="data" name="type" checked/><br>
                    <label for="general">General&nbsp;</label><input id="general" type="radio" value="general" name="type" checked/><br><br>
                    <button type='button' onclick='submitFeedback()'>Submit</button>
                </form>
            </div>
        `;
    }
}
customElements.define('lincs-feedback-form', lincsFeedbackForm);

function submitFeedback() {
    var name = document.getElementById('name').value.replace(/\s/g, '');
    var email = document.getElementById('email').value.replace(/\s/g, '');
    var title = document.getElementById('title').value.replace(/\s/g, '');
    var desc = document.getElementById('description').value.replace(/\s/g, '');

    if (name == "" || email == "" || title == "" || desc == "") {
        alert("Feedback Form Incorrect");
    } else {
        var form = document.getElementById('lincsFeedbackForm');
        var xhr  = new XMLHttpRequest();
        var data = new FormData(form);


        xhr.onload = function() {
            var children = form.children;
            for (var i = 0; i < children.length; i++) {
                var child = children[i];
                if (child.type == "text" || child.name == "description") {
                    child.value = "";
                }
            }

            alert("Feedback Submitted Successfully");
        }

        xhr.open("post", form.action);
        xhr.send(data);
    }
}