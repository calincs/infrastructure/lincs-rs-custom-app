var setInnerHTML = function(elm, html) {
    elm.innerHTML += html;
    Array.from(elm.querySelectorAll("script")).forEach( oldScript => {
        const newScript = document.createElement("script");
        newScript.async = false;
        Array.from(oldScript.attributes).forEach( attr => newScript.setAttribute(attr.name, attr.value) );
        newScript.appendChild(document.createTextNode(oldScript.innerHTML));
        oldScript.parentNode.replaceChild(newScript, oldScript);
    });
}

class lincsFeedbackImport extends HTMLElement {
    constructor() {
        super();

        if (!document.getElementById("lincs-imports").innerHTML.includes("lincs-feedback-form.js")) {
            setInnerHTML(document.getElementById("lincs-imports"), `
                                                                    <script src="/assets/components/feedback/lincs-feedback-form.js"></script>
                                                                    `);
        }
    }
}
customElements.define('import-lincs-feedback-form', lincsFeedbackImport);

class lincsTimelineImport extends HTMLElement {
    constructor() {
        super();

        if (!document.getElementById("lincs-imports").innerHTML.includes("lincs-timeline.js")) {
            setInnerHTML(document.getElementById("lincs-imports"), `
                                                                    <link href="/assets/components/timeline/vertical-timeline.css" rel="stylesheet">
                                                                    <script src="/assets/components/timeline/lincs-timeline.js"></script>
                                                                    `);
        }
    }
}
customElements.define('import-lincs-timline', lincsTimelineImport);