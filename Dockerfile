ARG TAG
FROM registry.gitlab.com/calincs/infrastructure/researchspace-fork:${TAG}
USER root
COPY --chown=100:0 ./lincs /apps/lincs/
ENTRYPOINT [ "/apps/lincs/entrypoint.sh" ]
