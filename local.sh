#!/bin/sh

docker build . -t local/researchspace --build-arg TAG=v8.1.3

kubectl get namespace researchspace || kubectl create namespace researchspace

kubectl -n researchspace apply -f local-env.yaml

echo "Running helm upgrade..."
helm -n researchspace upgrade --install researchspace ./chart \
  --wait -f local-values.yaml

#helm uninstall researchspace -n researchspace
